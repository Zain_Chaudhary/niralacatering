import 'dart:io';
import 'dart:io' as io;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppIcons.dart';
import 'package:permission_handler/permission_handler.dart';

final FirebaseAuth _uth = FirebaseAuth.instance;

class AddressProvider extends ChangeNotifier {
  String _userAddress;

  String get userAddress => _userAddress;

  setUserAddress(String address) {
    _userAddress = address;
    notifyListeners();
  }


  openBottomSheet(context) {
    showModalBottomSheet(
        isDismissible: false,
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:  [
                    Icon(
                      AppIcons.arrowDown,
                      color: ColorConstant.appMainColor,
                      size: 40,
                    )
                  ],
                ),
                ListTile(
                    leading:  Icon(AppIcons.camera,
                      color: ColorConstant.appMainColor,
                    ),
                    title: const Text('From CAMERA'),
                    onTap: () async {
                      // imgFrom(ImageSource.camera, context);
                      // Navigator.of(context).pop();
                      if(permissionStatus==_permissionStatus){
                        imgFrom(ImageSource.camera, context);
                      Navigator.of(context).pop();
                      }
                      else {
                        if (await requestPermission() == _permissionStatus.isGranted) {
                          imgFrom(ImageSource.camera, context);
                          Navigator.of(context).pop();
                        }
                      }
                    }),
                ListTile(
                  leading:  Icon(AppIcons.gallery,
                    color: ColorConstant.appMainColor,
                  ),
                  title: const Text('From INTERNAL STORAGE'),
                  onTap: () async {
                    // imgFrom(ImageSource.gallery, context);
                    // Navigator.of(context).pop();
                    if(permissionStatus==_permissionStatus){
                      imgFrom(ImageSource.gallery, context);
                      Navigator.of(context).pop();
                    }
                    else {
                      if (await requestPermission() == _permissionStatus.isGranted) {
                        imgFrom(ImageSource.gallery, context);
                        Navigator.of(context).pop();
                      }
                    }
                  },
                ),
              ],
            ),
          );
        });
  }

  File profilePic;
  String ImgUrl;
  Future imgFrom(ImageSource imageSource, BuildContext context) async {
    User user = await _uth.currentUser;
    String id = user.uid;

    PickedFile pickedFile;
    ImagePicker picker = ImagePicker();
    pickedFile = await picker.getImage(source: imageSource);
    profilePic = File(pickedFile.path);
    firebase_storage.Reference firebaseStorageRef =
        firebase_storage.FirebaseStorage.instance.ref('profilePics').child(id);
    firebase_storage.UploadTask uploadTask =
        firebaseStorageRef.putFile(io.File(pickedFile.path));
    uploadTask.then((res) async {
      ImgUrl = await res.ref.getDownloadURL();
      print(ImgUrl);

      notifyListeners();
    });
    notifyListeners();
  }

  PermissionStatus _permissionStatus;
  PermissionStatus permissionStatus;
  requestPermission() async {
    _permissionStatus = await Permission.storage.status;

    if (_permissionStatus != PermissionStatus.granted) {
      permissionStatus = await Permission.storage.request();
      _permissionStatus = permissionStatus;
      notifyListeners();
      return permissionStatus;
    }
  }
}
