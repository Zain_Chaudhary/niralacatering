import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nirala/Components/ShowToast/ShowToast.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';

final database = FirebaseDatabase.instance.reference();
final FirebaseAuth _uth = FirebaseAuth.instance;

class RestaurantMenuProvider extends ChangeNotifier {
  bool isLoading = true;
  List selectMenuData;

  void getSelectMenuData(String selectMenuName) {
    isLoading = true;
    database
        .child(KeysConstants.menuData)
        .child(selectMenuName)
        .once()
        .then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> map = snapshot.value;
      selectMenuData = map.values.toList();
      isLoading = false;
      notifyListeners();
    });
    notifyListeners();
  }

  int count = 1;
  int productnextpass;
  String productname;
  String productprice;
  String productimage;
  String productdes;
  String productcal;
  String productweight;
  String deliverytime;

  Future<void> getMenuDetail(String productname, name, menu,bool isDealMenu) async {
    isLoading = true;
    if (isDealMenu) {
      await database
          .child(menu)
          .child(productname)
          .once()
          .then((DataSnapshot snapshot) {
        var data = snapshot.value;
        productimage = data['productimg'];
        productdes = data['productdes'];
        productprice = data['productprice'];
        productcal = data['productcalo'];
        productweight = data['productweight'];
        deliverytime = data['producttime'];
        productnextpass = int.parse(data['productprice']);
        isLoading = false;
        notifyListeners();
      });
    } else {
      await database
          .child(menu)
          .child(name)
          .child(productname)
          .once()
          .then((DataSnapshot snapshot) {
        var data = snapshot.value;
        productimage = data['productimg'];
        productdes = data['productdes'];
        productprice = data['productprice'];
        productcal = data['productcal'];
        productweight = data['productweight'];
        deliverytime = data['producttime'];
        productnextpass = int.parse(data['productprice']);
        isLoading = false;
        notifyListeners();
      });
    }
    notifyListeners();
  }

  void addItemToCart(BuildContext context , String productname, name, menu) async{
    User user = _uth.currentUser;
    String id = user.uid;
    if (count != 0) {
      database
          .child("additemincart")
          .child(id)
          .child(productname)
          .update({
        'productmenu': menu,
        'productcat': name,
        'productimg': productimage,
        'deliverytime': deliverytime,
        'porignalprice': productprice,
        'productname': productname,
        'producttotalprice': productnextpass.toString(),
        'totalproductselect': count.toString(),
        'userid': id,
      });
      Navigator.of(context)
          .pushNamed(RouteConstants.userOrderMenuCartUI);
    } else {
      Fluttertoast.showToast(
          msg: "Please Select Number of Product",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 3,
          backgroundColor: ColorConstant.appMainColor,
          textColor: Colors.black,
          fontSize: 16.0);
    }
  }

  void userFavoriteMenu(String productName) {
    User user = _uth.currentUser;
    String id = user.uid;

    database.child('favorite').child(id).child(productName).update({
      'productname': productName,
      'productimg': productimage,
      'productprice': productprice,
    });
    showToast('Successfully add',color: ColorConstant.green);
  }
}
