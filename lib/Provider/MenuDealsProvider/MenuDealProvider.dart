import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';

final database = FirebaseDatabase.instance.reference();

class MenuDealProvider extends ChangeNotifier {
  List data;
  bool isLoading = true;

  Future<dynamic> getMenuDealsData() async {
    database
        .child(KeysConstants.dealOfDay)
        .once()
        .then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> map = snapshot.value;
      data = map.values.toList();
      isLoading = false;
      notifyListeners();
    });
  }
}
