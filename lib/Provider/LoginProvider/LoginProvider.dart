import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Services/CheckUserAddressService/CheckUserAddress.dart';

final FirebaseAuth _uth = FirebaseAuth.instance;
final database = FirebaseDatabase.instance.reference();

class LoginProvider extends ChangeNotifier {
  TextEditingController phoneNoController = new TextEditingController();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool isLoading = true;



  Future<void> checkUserLoginInApp(BuildContext context) async {
    User user = await _uth.currentUser;
    if (user != null) {
      CheckUserAddress.checkUserAddress(context);
      Timer(Duration(seconds: 1), () => isLoading=false);
      notifyListeners();
    } else {
      isLoading=false;
      notifyListeners();
    }
  }

  Future<void> updateUserLoginDetail() async {
    User user = await _uth.currentUser;
    String id = user.uid;

    database.child("userphonenumber").child(id).update({
      'userphonenumber': '+92' + phoneNoController.text.toString(),
      'userid': id,
    });
  }
}
