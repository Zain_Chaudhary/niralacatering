import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';

final database = FirebaseDatabase.instance.reference();

class HomeProvider extends ChangeNotifier {
  bool isLoading = true;
  List dealMenuData;
  List suggestionMenuData;
  String todayTime;

  setLoading(bool setLoading) {
    isLoading = setLoading;
    notifyListeners();
  }



  void getDealsMenuData() {
    database
        .child(KeysConstants.dealOfDay)
        .once()
        .then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> map = snapshot.value;
      dealMenuData = map.values.toList();
      getSuggestionMenuData();
      getTodayTime();
      notifyListeners();
    });
    notifyListeners();
  }

  void getSuggestionMenuData() {
    database.child('suggestion').once().then((DataSnapshot snapshot) {
      var data = snapshot.value;

      String suggestionName = data['suggestioncate'];

      database
          .child(KeysConstants.menuData)
          .child(suggestionName)
          .once()
          .then((DataSnapshot snapshot) {
        Map<dynamic, dynamic> map = snapshot.value;
        suggestionMenuData = map.values.toList();
        setLoading(false);
        notifyListeners();
      });
      notifyListeners();
    });
    notifyListeners();
  }
  String getTodayTime() {
    var timeNow = DateTime
        .now()
        .hour;

    if (timeNow <= 12) {
      return todayTime = 'Good Morning';
    } else if ((timeNow > 12) && (timeNow <= 16)) {
      return todayTime = 'Good Afternoon';
    } else if ((timeNow > 16) && (timeNow < 20)) {
      return todayTime = 'Good Evening';
    } else {
      return todayTime = 'Good Night';
    }
  }
}
