import 'package:flutter/material.dart';
import 'package:nirala/Components/Buttons/FilledButton.dart';
import 'package:nirala/UI/AppLoginUI/LoginUI.dart';

class GuestFCLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xFFFFBD2F),
        elevation: 0,
        title: Center(
            child: Text(
          'Account Login',
          style: TextStyle(color: Colors.black, fontFamily: 'heading'),
        )),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Color(0xFFFFBD2F),
          ),
        ),
      ),
      body: FavoritePage(),
    );
  }
}

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.asset(
              'assets/images/gifs/fav.gif',
              height: height * 0.2,
            ),
          ),
          SizedBox(
            height: height * 0.05,
          ),
          Container(
            width: width * 0.5,
            height: height * 0.09,
            child: FilledButton(
              onPressed:() =>     Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => AppLoginUI())),
              title: 'Login  First',
            ),
          ),
        ],
      ),
    );
  }
}
