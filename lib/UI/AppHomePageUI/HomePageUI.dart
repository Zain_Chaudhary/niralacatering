import 'package:nirala/Components/AppLoader/AppLoader.dart';
import 'package:nirala/Components/Widgets/HomeUIWidgets/HomeDealMenu.dart';
import 'package:nirala/Components/Widgets/HomeUIWidgets/HomeNearYouRestaurant.dart';
import 'package:nirala/Components/Widgets/HomeUIWidgets/HomeSuggestionMenu.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Models/HomePageModel/HomePageCategoryModel.dart';
import 'package:nirala/Provider/HomeProvider/HomeProvider.dart';
import 'package:nirala/UI/RestaurantMenu/SelectMenuCategory.dart';
import 'package:provider/provider.dart';

class AppHomePageUI extends StatefulWidget {
  @override
  State<AppHomePageUI> createState() => _AppHomePageUIState();
}

class _AppHomePageUIState extends State<AppHomePageUI> {



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<HomeProvider>(context, listen: false).getDealsMenuData();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
        backgroundColor: Colors.white,
        body: Consumer<HomeProvider>(
            builder: (context, homeProvider, child) {
              return SafeArea(
                child: homeProvider.isLoading
                    ? AppLoader()
                    : SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(
                        top: size.height * 0.01,
                        left: size.width * 0.04,
                        right: size.width * 0.04),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: size.height * 0.01),
                          child: Text(
                            homeProvider.todayTime,
                            style: FcHomeGoodMoring,
                          ),
                        ),
                        // Container(
                        //   margin: EdgeInsets.only(
                        //       right: width * 0.02, top: height * 0.01),
                        //   child: TextField(
                        //     keyboardType: TextInputType.text,
                        //     decoration: InputDecoration(
                        //         hintText: 'Search',
                        //         hintStyle: FCHomeSearch,
                        //         fillColor: Color(0xfff8f8f8),
                        //         filled: true,
                        //         contentPadding: EdgeInsets.only(
                        //             left: width * 0.02, top: height * 0.02),
                        //         border: InputBorder.none,
                        //         focusedBorder: InputBorder.none,
                        //         enabledBorder: InputBorder.none,
                        //         errorBorder: InputBorder.none,
                        //         prefixIcon: Icon(
                        //           Icons.search_sharp,
                        //           color: Colors.black,
                        //           size: 20,
                        //         )),
                        //   ),
                        // ),
                        Container(
                          margin: EdgeInsets.only(top: size.height * 0.01),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: size.height * 0.01),
                                child: Text(
                                  'Category',
                                  style: FCHomeCategory,
                                ),
                              ),
                              Container(
                                width: size.width * 1.0,
                                height:size. height * 0.2,
                                child: ListView.builder(
                                  itemCount: homePageCategoryList == null
                                      ? 0
                                      : homePageCategoryList.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (BuildContext context,
                                      int index) {
                                    return InkWell(
                                      onTap: () =>
                                          Navigator.of(context)
                                              .push(MaterialPageRoute(
                                              builder: (context) =>
                                                  SelectMenuCategory(
                                                    categoryName: homePageCategoryList[index]
                                                        .name,
                                                    categoryImage: homePageCategoryList[index].categoryImage,
                                                  ))),
                                      child: Container(
                                        margin: EdgeInsets.only(right: 10),
                                        child: Column(
                                          children: [
                                            Card(
                                              elevation: 16,
                                              shadowColor: Colors.white,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      60)),
                                              child: Container(
                                                  padding: EdgeInsets.all(3),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Color(0xFFFFBD2F),
                                                  ),
                                                  child: ClipOval(
                                                    child: Image.asset(
                                                      homePageCategoryList[index]
                                                          .categoryImage,
                                                      height: 70,
                                                      width: 70,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  )),
                                            ),
                                            SizedBox(
                                              height:size. height * 0.01,
                                            ),
                                            Container(
                                                child: Text(
                                                  homePageCategoryList[index]
                                                      .name,
                                                  style: FCHomeCategoryTitle,
                                                )),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.01,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: size.height * 0.01),
                              child: Text('Suggestion for you ',
                                style: FCHomeSuggested,),
                            ),
                            Container(
                              height: size.height * 0.45,
                              width: size.width * 1.0,
                              child: HomeSuggestionMenu(homeProvider: homeProvider,)
                            ),
                            SizedBox(
                              height: size.height * 0.02,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    margin:
                                    EdgeInsets.only(bottom: size.height * 0.01),
                                    child: Text(
                                      'Deal of the day',
                                      style: FCHomeDealDay,
                                    )),
                                HomeDealMenu(homeProvider: homeProvider,),
                              ],
                            ),
                            SizedBox(
                              height: size.height * 0.01,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin:
                                  EdgeInsets.only(bottom: size.height * 0.01),
                                  child: Text(
                                    'Near You',
                                    style: FCHomeNear,
                                  ),
                                ),
                                Container(
                                  width: size.width * 0.99,
                                  height: size.height * 0.5,
                                  margin:
                                  EdgeInsets.only(bottom: size.height * 0.01),
                                  child: HomeNearYouRestaurant()
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
        ),
    );
  }
}

