import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:nirala/Components/AppDrawer/AppDrawer.dart';
import 'package:nirala/Components/CustomImages/NetworkImage.dart';
import 'package:nirala/Components/ShowToast/ShowToast.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';

final database = FirebaseDatabase.instance.reference();
final FirebaseAuth _auth = FirebaseAuth.instance;

class UserFavFoodUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FavoritePage(),
    );
  }
}

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  var favorite = 'favorite';
  String productname;

  List favData;

  void getFavItem() {
    User user = _auth.currentUser;
    String id = user.uid;
    database.child(favorite).child(id).once().then((DataSnapshot snapshot) {
      setState(() {
        Map<dynamic, dynamic> map = snapshot.value;
        favData = map.values.toList();
      });
    });
  }

  void _delitemincart(String pname, int i) {
    User user = _auth.currentUser;
    String id = user.uid;

    database.child(favorite).child(id).child(pname).remove();
    setState(() {
      favData.removeAt(i);
      if (favData != null) {
        showToast('Item Delete From Favorite');
      } else {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => AppDrawer()));
      }
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getFavItem();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return favData != null
        ? SingleChildScrollView(
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: EdgeInsets.only(
                        top: size.height * 0.03,
                        bottom:size. height * 0.01,
                        left: size.width * 0.05,
                      ),
                      child: Text(
                        'Favourite',
                        style: FCHomeDealDay,
                      )),
                  Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: favData == null ? 0 : favData.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {},
                          child: Container(
                            padding: EdgeInsets.only(
                                left: size.width * 0.05, right: size.width * 0.05),
                            height: size.height * 0.35,
                            width: size.width * 0.92,
                            margin:
                                EdgeInsets.only(bottom:size. height * 0.02),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(7),
                                topRight: Radius.circular(7),
                              ),
                            ),
                            child: Card(
                              elevation: 15,
                              shadowColor: Colors.white,
                              child: Column(
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Stack(
                                      children: [
                                        Container(
                                          height:size. height * 0.28,
                                          width: size.width * 0.92,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.only(
                                              topLeft: Radius.circular(7),
                                              topRight:
                                                  Radius.circular(7),
                                            ),
                                          ),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.only(
                                              topLeft: Radius.circular(7),
                                              topRight:
                                                  Radius.circular(7),
                                            ),
                                            child: appNetworkImage(favData[index]['productimg'], size.width * 0.92, size. height * 0.28, BoxFit.cover)

                                            ),
                                          ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                _delitemincart(
                                                    favData[index][
                                                            'productname']
                                                        .toString(),
                                                    index);
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    right: size.width * 0.04),
                                                width: size.width * 0.10,
                                                height: size.height * 0.10,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color:
                                                      Color(0xFFFFBD2F),
                                                ),
                                                child: GestureDetector(
                                                  onTap: () {
                                                    _delitemincart(
                                                        favData[index][
                                                                'productname']
                                                            .toString(),
                                                        index);
                                                  },
                                                  child: Icon(
                                                    Icons.delete_outlined,
                                                    color: Colors.white,
                                                    size: 25,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: size.height * 0.02),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          child: Text(
                                            favData[index]['productname']
                                                .toString(),
                                            style: TextStyle(
                                                fontSize: size.width * 0.05,
                                                fontFamily:
                                                    'heading'),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                              'Rs: ' +
                                                  favData[index]
                                                      ["productprice"] +
                                                  '/-',
                                              style: TextStyle(
                                                  fontSize: size.width * 0.045,
                                                  fontFamily:
                                                      'heading',
                                                  color:
                                                      Color(0xFFFFBD2F))),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: size.height * 0.05,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => AppDrawer()));
                    },
                    child: Container(
                      // width: 20,
                      // height: 20,
                      padding: EdgeInsets.only(
                          left: size.width * 0.05, right:size. width * 0.05),
                      child: Row(
                        children: [
                          Icon(
                            Icons.arrow_back_ios_sharp,
                            size: 18,
                          ),
                          Text(
                            'BACK',
                            style:
                                TextStyle(fontFamily: 'heading'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        : Column(
            children: [
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                  top: size.height * 0.3,
                ),
                child: Image.asset(
                  'assets/images/gifs/fav.gif',
                  height: size.height * 0.2,
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: size.height * 0.05),
                child: Text(
                  'Nothing to show',
                  style: FCFavoriteTitle,
                ),
              )
            ],
          );
  }
}
