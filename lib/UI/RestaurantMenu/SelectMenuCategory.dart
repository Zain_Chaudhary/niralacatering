import 'package:nirala/Components/AppLoader/AppLoader.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Provider/ResturantMenuProvider/RestaurantMenuProvider.dart';
import 'package:nirala/UI/RestaurantMenu/SelectMenuDetail.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppIcons.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:provider/provider.dart';

class SelectMenuCategory extends StatefulWidget {
  String categoryName;
  String categoryImage;

  SelectMenuCategory({this.categoryName, this.categoryImage});

  @override
  State<SelectMenuCategory> createState() => _SelectMenuCategoryState();
}

class _SelectMenuCategoryState extends State<SelectMenuCategory> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<RestaurantMenuProvider>(context, listen: false)
        .getSelectMenuData(widget.categoryName);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstant.appMainColor,
        elevation: 0,
        leading: IconButton(
          icon: Icon(AppIcons.arrowBack),
          onPressed: () => Navigator.of(context).pop(),
          color: Colors.black,
        ),
        title: Center(
          child: Text(
            widget.categoryName,
            style: FCMenuTitle,
          ),
        ),
      ),
      body: Consumer<RestaurantMenuProvider>(
          builder: (context, restaurantMenuProvider, child) {
        return SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: size.height * 0.8,
                width: size.width * 1.0,
                decoration: BoxDecoration(
                  color: Colors.black,
                  image: DecorationImage(
                    image: AssetImage(widget.categoryImage),
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.5), BlendMode.dstATop),
                  ),
                  // borderRadius: BorderRadius.circular(15),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: size.height * 0.08, left: size.width * 0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: size.height * 0.05,
                    ),
                    Text(widget.categoryName, style: FCsignalMenuname),
                    SizedBox(
                      height: size.height * 0.02,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: size.height * 0.25),
                width: size.width * 1.0,
                height: size.height * 0.7,
                decoration: BoxDecoration(
                    color: ColorConstant.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      Text(
                        "${KeysConstants.appName} Restaurant Menu",
                        style: FCsignalMenuheadingText,
                      ),
                      SizedBox(
                        height: size.height * 0.02,
                      ),
                      restaurantMenuProvider.isLoading
                          ? AppLoader()
                          : ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount:
                                  restaurantMenuProvider.selectMenuData == null
                                      ? 0
                                      : restaurantMenuProvider
                                          .selectMenuData.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  alignment: Alignment.center,
                                  width: size.width * 1.0,
                                  height: size.height * 0.2,
                                  child: Stack(
                                    children: [
                                      Container(
                                        width: size.width * 0.9,
                                        height: size.height * 0.17,
                                        margin: EdgeInsets.only(
                                            top: size.height * 0.04,
                                            left: size.width * 0.02),
                                        child: Card(
                                          elevation: 12,
                                          shadowColor: ColorConstant.white,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(100),
                                            bottomRight: Radius.circular(50),
                                          )),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    width: size.width * 0.22,
                                                    height: size.height * 0.05,
                                                    child: RaisedButton(
                                                      elevation: 6,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  18),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  18),
                                                        ),
                                                      ),
                                                      onPressed: () => Navigator
                                                              .of(context)
                                                          .push(
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          MenuDetail(
                                                                            productname:
                                                                                restaurantMenuProvider.selectMenuData[index]['productname'],
                                                                            name:
                                                                                restaurantMenuProvider.selectMenuData[index]['productcat'],
                                                                            menu:
                                                                                KeysConstants.menuData,
                                                                          ))),
                                                      color: ColorConstant
                                                          .appMainColor,
                                                      child: Text(
                                                        'Order',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'heading'),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: size.width * 0.05,
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    left: size.width * 0.2),
                                                child: Text(
                                                  restaurantMenuProvider
                                                      .selectMenuData[index]
                                                          ['productname']
                                                      .toString(),
                                                  style:
                                                      FCsignalMenusubheadingText,
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left:
                                                            size.width * 0.08),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          '(' +
                                                              restaurantMenuProvider
                                                                          .selectMenuData[
                                                                      index][
                                                                  'productrating'] +
                                                              ')',
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  'poppinTextStyle',
                                                              color: ColorConstant.appMainColor),
                                                        ),
                                                        Container(
                                                          child: Icon(
                                                            Icons.star,
                                                            color: ColorConstant.appMainColor,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                          right: size.width *
                                                              0.08),
                                                      alignment:
                                                          Alignment.topRight,
                                                      child: Text(
                                                        'Rs ' +
                                                            restaurantMenuProvider
                                                                        .selectMenuData[
                                                                    index][
                                                                'productprice'] +
                                                            '/-'.toString(),
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            fontFamily:
                                                                'poppinTextStyle',
                                                            color: ColorConstant.appMainColor),
                                                      )),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: size.width * 0.07,
                                            top: size.height * 0.01),
                                        child: Card(
                                          elevation: 12,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      size.height * 0.07)),
                                          child: Container(
                                            padding: EdgeInsets.all(3),
                                            decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey[300],
                                                  blurRadius: 2,
                                                  spreadRadius: 1,
                                                  offset: Offset(0, 3),
                                                )
                                              ],
                                              shape: BoxShape.circle,
                                              color: ColorConstant.appMainColor,
                                            ),
                                            child: CircleAvatar(
                                              radius: size.height * 0.06,
                                              backgroundImage: NetworkImage(
                                                restaurantMenuProvider
                                                    .selectMenuData[index]
                                                        ['productimg']
                                                    .toString(),
                                              ),
                                              backgroundColor: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                      SizedBox(
                        height: size.height * 0.08,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
