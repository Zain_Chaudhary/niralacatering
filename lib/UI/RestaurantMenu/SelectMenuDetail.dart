
import 'package:nirala/Components/AppLoader/AppLoader.dart';
import 'package:nirala/Components/CustomImages/NetworkImage.dart';
import 'package:nirala/Components/ShowToast/ShowToast.dart';
import 'package:nirala/Constant Folder/Constant File.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nirala/Provider/ResturantMenuProvider/RestaurantMenuProvider.dart';
import 'package:nirala/UI/FavFoodUI/UserFavFoodUI.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';


class MenuDetail extends StatefulWidget {
  final String productname;
  final String name;
  final String menu;
  bool isDealMenu;

  MenuDetail({this.productname, this.name, this.menu,this.isDealMenu=false});

  @override
  State<MenuDetail> createState() => _MenuDetailState();
}

class _MenuDetailState extends State<MenuDetail> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<RestaurantMenuProvider>(context,listen: false).getMenuDetail(widget.productname, widget.name, widget.menu,widget.isDealMenu);
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFFFBD2F),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFFFBD2F),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.share_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Share.share(
                  'https://play.google.com/store/apps/details?id=com.haythamtech.nirala');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.favorite_border_sharp,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => UserFavFoodUI()));
            },
          )
        ],
      ),
      body: Consumer<RestaurantMenuProvider>(
        builder: (context, restaurantMenuProvider,child) {
          return restaurantMenuProvider.isLoading
            ? AppLoader()
            : SingleChildScrollView(
          child: Container(
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: size.width * 0.03),
                  child: Text(
                    widget.name,
                    style: FCProductTags,
                  ),
                ),
                Container(
                  margin:
                  EdgeInsets.only(top: size.height * 0.04, left: size.width * 0.03,right: size.width*0.02),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(

                        child: Text(
                          widget.productname.toString(),
                          style: FCProductName,
                        ),
                      ),
                      Container(

                        child: Text(
                          'Rs: '+ restaurantMenuProvider.productprice + '/-',
                          style: TextStyle(fontSize: 19, fontFamily: 'heading'),
                        ),
                      ),

                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.07),
                  child: Image.asset('assets/images/showproductBG.png'),
                ),
                Container(
                  height: size.height * 0.6,
                  width: size.width * 1.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  margin: EdgeInsets.only(top: size.height * 0.3),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1),
                      margin: EdgeInsets.only(top: size.height * 0.2),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60),
                          color: Colors.white,
                          border: Border.all(color: Colors.white, width: 2)),
                      child: ClipOval(
                        child: appNetworkImage(restaurantMenuProvider.productimage,120, 120, BoxFit.cover),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: size.height*0.31,left: size.width*0.06,right: size.width*0.06),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          height: size.height * 0.1,
                          width: size.width * 0.2,
                          decoration: BoxDecoration(
                            //    color: Color(0xFFFFBD2F),
                              borderRadius: BorderRadius.circular(25)),
                          child:Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                child: Text('Calories',
                                  style: TextStyle(fontFamily: 'poppinTextStyle'),
                                ),
                              ),
                              Divider(
                                indent: 10,
                                endIndent: 10,
                                thickness: 1,
                                color: Colors.grey,
                              ),
                              Container(
                                child:  Text(restaurantMenuProvider.productcal + ' kkal',
                                    style: FCProductRatio),
                              )
                            ],
                          )
                      ),
                      Container(

                          height: size.height * 0.1,
                          width: size.width * 0.2,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25)),
                          child:Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                child: Text('Weight',
                                  style: TextStyle(fontFamily: 'poppinTextStyle'),
                                ),
                              ),
                              Divider(
                                indent: 15,
                                endIndent: 13,
                                thickness: 1,
                                color: Colors.grey,
                              ),
                              Container(
                                child:  Text(restaurantMenuProvider.productweight + ' gr',
                                    style: FCProductRatio),
                              )
                            ],
                          )
                      ),

                    ],
                  ),
                ),

                Positioned(
                  top: size.height * 0.46,
                  child: Container(
                    margin: EdgeInsets.only(
                      left: size.width * 0.03,
                    ),
                    child: Text(
                      'Descriptions',
                      style: FCProductDescTitle,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Positioned(
                  top: size.height * 0.52,
                  child: Container(
                    width:size. width * 0.9,
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(
                        left: size.width * 0.03, right: size.width * 0.03),
                    child: Text(
                      restaurantMenuProvider.productdes,
                      style: FCProductDesc,
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                Positioned(
                  top: size.height * 0.64,
                  child: Container(
                    margin: EdgeInsets.only(left: size.width * 0.03),
                    child: Row(
                      children: [
                        Container(
                          width: size.width * 0.5,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin:
                                EdgeInsets.only(bottom: size.height * 0.01),
                                child: Text(
                                  'TotalPrice',
                                  style: FCProductPrice,
                                ),
                              ),
                              Container(
                                child: Text(
                                  'Rs: ' + restaurantMenuProvider.productnextpass.toString() + '/-',
                                  style: FCProductPriceRs,
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: size.width * 0.5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if(restaurantMenuProvider.count>1){
                                      restaurantMenuProvider.count--;
                                      var productrs = int.parse(restaurantMenuProvider.productprice);
                                      restaurantMenuProvider.productnextpass -=productrs;
                                    }else{
                                      showToast('No more',color: ColorConstant.red);
                                    }
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    shape: BoxShape.circle,
                                  ),
                                  child: Text('-', style: FCProductIncrement),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: size.width * 0.02, right: size.width * 0.02),
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    color: Color(0xFFFFBD2F),
                                    shape: BoxShape.circle),
                                child: Text(restaurantMenuProvider.count.toString(),
                                    style: FCProductIncrement),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    restaurantMenuProvider.count++;
                                    var productrs = int.parse(restaurantMenuProvider.productprice);
                                    restaurantMenuProvider.productnextpass += productrs;
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: Colors.grey[100],
                                      shape: BoxShape.circle),
                                  child: Text(
                                    '+',
                                    style: FCProductIncrement,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: size.height * 0.77,
                  child: Container(
                    width: size.width * 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          height: size.height * 0.07,
                          width: size.width * 0.13,
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: IconButton(
                            onPressed: ()=>restaurantMenuProvider.userFavoriteMenu(widget.productname),
                            icon: Icon(
                              Icons.favorite_border,
                              color: Colors.grey,
                              size: 19,
                            ),
                          ),
                        ),
                        Container(
                          height: size.height * 0.07,
                          width:size. width * 0.6,
                          child: RaisedButton.icon(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                            elevation: 3,
                            label: Text(
                              'Add to Bucket',
                              style: FCProductAddBucket,
                            ),
                            icon: FaIcon(
                              FontAwesomeIcons.shoppingBasket,
                              size: 20,
                            ),
                            onPressed: ()=>restaurantMenuProvider.addItemToCart(context, widget.productname, widget.name, widget.menu,),
                            color: ColorConstant.appMainColor,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
    );
        }
      ),
    );
  }
}


