import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Models/MainMenuModel/MainMenuModel.dart';
import 'package:nirala/UI/RestaurantMenu/SelectMenuCategory.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';

class ResturantMainMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: ColorConstant.appMainColor,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(
                  top: size.height * 0.05,
                  left: size.width * 0.04,
                  right: size.width * 0.04,
                  bottom: size.height * 0.05),
              child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount:
                      mainMenuModelList == null ? 0 : mainMenuModelList.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: .95,
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 15),
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: ()=>   Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SelectMenuCategory(
                            categoryName: mainMenuModelList[index].name,
                            categoryImage: mainMenuModelList[index].categoryImage,
                          ))),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: DecorationImage(
                            image: AssetImage(
                                mainMenuModelList[index].categoryImage),
                            fit: BoxFit.cover,
                            colorFilter: ColorFilter.mode(
                                Colors.black.withOpacity(0.5),
                                BlendMode.dstATop),
                          ),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child:    Center(
                          child: Text(
                            '${mainMenuModelList[index].name}',
                            style: FCMenuSubTitle,
                          ),
                        ) ,
                      ),
                    );
                  }),
            ),
          ),
        ));
  }
}
