import 'package:analog_clock/analog_clock.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:nirala/Components/Buttons/FilledButton.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';


final database = FirebaseDatabase.instance.reference();
final FirebaseAuth _uth = FirebaseAuth.instance;
class OrderTrackerTimer extends StatefulWidget {
  @override
  _OrderTrackerTimerState createState() => _OrderTrackerTimerState();
}

class _OrderTrackerTimerState extends State<OrderTrackerTimer> {

  bool isActive=false;

String time;


  Future<void> _Tracker() async {
    User user = _uth.currentUser;
    String id = user.uid;
   await database.child('tracker').child(id).once().then((DataSnapshot snapshot) {
      var data = snapshot.value;

      setState(() {
        time = data['time'].toString();
     isActive=data['status'];

      });
    });
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _Tracker();
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async{
      Navigator.of(context).pushReplacementNamed(RouteConstants.appDrawer);
        return false;
      },
      child: Scaffold(
            backgroundColor: Colors.white,
            body:  isActive == false
                ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(child: Text('Wait For Order Accept')),
                    //AppLoader(),
                  ],
                )
                :SingleChildScrollView(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children:[
                        Center(
                          child: Container(
                            width: size.width*0.5,
                            height: size.height*0.5,
                            child: AnalogClock(
                              decoration: BoxDecoration(
                                  border: Border.all(width: 3.0, color: Colors.black),
                                  color: Colors.black,
                                  shape: BoxShape.circle),
                              width: 200.0,
                              isLive: true,
                              hourHandColor: Colors.white,
                              minuteHandColor: Colors.white,
                              secondHandColor: Color(0xFFFFBD2F),
                              showSecondHand: true,
                              numberColor: Colors.white,
                              showNumbers: true,
                              textScaleFactor: 1.5,
                              showTicks: true,
                              showDigitalClock: true,
                              digitalClockColor: Colors.white,
                              datetime: DateTime.now(),
                            ),
                          ),
                        ),

                        SizedBox(
                          height:size.height*0.0545,
                        ),
                        Center(
                          child: Container(
                            width:size. width*0.9,
                            child: Text('Total Time '+time+ 'min Required to Delivery Your Order',
                            textAlign: TextAlign.center,
                            style: FCSummaryOrderPrice,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Center(
                          child: Container(
                            child: Text('Thank You',
                            style: FCSummaryOrderPrice,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        FilledButton(
                          width: size.width*0.5,
                          height: 45,
                          title: 'Home',
                          onPressed: ()=>Navigator.of(context).pushReplacementNamed(RouteConstants.appDrawer),
                        ),
                      ],
                    ),
                  ),
                ),
          ),
    );
  }
}
