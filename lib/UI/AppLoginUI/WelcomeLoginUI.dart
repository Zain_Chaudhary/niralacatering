import 'package:nirala/Components/AppLoader/AppLoader.dart';
import 'package:nirala/Components/Buttons/FilledButton.dart';
import 'package:nirala/Components/Buttons/OutlinedButton.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Provider/LoginProvider/LoginProvider.dart';
import 'package:nirala/UI/AppGuestUserUI/BottomNavigationBar.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppImages.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';
import 'package:provider/provider.dart';

class AppWelcomeLoginUI extends StatefulWidget {
  @override
  State<AppWelcomeLoginUI> createState() => _AppWelcomeLoginUIState();
}

class _AppWelcomeLoginUIState extends State<AppWelcomeLoginUI> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<LoginProvider>(context, listen: false)
        .checkUserLoginInApp(context);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Consumer<LoginProvider>(builder: (context, loginProvider, child) {
      return Scaffold(
        body: loginProvider.isLoading
            ? AppLoader()
            : Stack(
                children: [
                  Container(
                    width: size.width * 1.0,
                    height: size.height * 1.0,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/bkg.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          AppImages.appLogoRemoveBG,
                          width: size.width,
                          height: 150,
                        ),
                        SizedBox(height: 20),
                        FilledButton(
                          width: size.width * 0.8,
                          height: 45,
                          btnColor: ColorConstant.black,
                          textColor: ColorConstant.appMainColor,
                          onPressed: () => Navigator.of(context).pushNamed(RouteConstants.appLoginUI),
                          title: 'Sign In / Sign Up',
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomOutlineButton(
                          width: size.width * 0.8,
                          height: 45,
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => GuestBottomNavBar())),
                          title: 'Continue as Guest',
                          borderColor: ColorConstant.black,
                          textColor: ColorConstant.black,
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ],
              ),
      );
    });
  }
}
