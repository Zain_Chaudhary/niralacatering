import 'package:nirala/Components/AppHeader/AppHeader.dart';
import 'package:nirala/Components/Buttons/FilledButton.dart';
import 'package:nirala/Components/ShowToast/ShowToast.dart';
import 'package:nirala/Components/TextFields/CustomTextField.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Provider/LoginProvider/LoginProvider.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppIcons.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';
import 'package:provider/provider.dart';

class AppLoginUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Consumer<LoginProvider>(builder: (context, loginProvider, child) {
      return Scaffold(
        body: Stack(
          children: [
            Container(
              width: size.width * 1.0,
              height: size.height * 1.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/bkg.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        AppHeader(
                          title: 'Login',
                          onPressedLeadingButton: () =>
                              Navigator.of(context).pop(),
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: Text(
                            'Enter your phone number',
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontFamily: 'heading'),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: size.width * 0.9,
                          child: Text(
                            'Please enter your phone number. You will receive a code to verify mobile number.',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Form(
                          key: loginProvider.formKey,
                          child: Container(
                              width: size.width * 0.8,
                              child: CustomTextFormField(
                                controller: loginProvider.phoneNoController,
                                labelText: 'Phone number',
                                suffixWidget: Icon(AppIcons.phone),
                                prefixWidget: Padding(
                                  padding: const EdgeInsets.only(top: 15,left: 5),
                                  child: Text("+92"),
                                ),
                                validator: (String value) {
                                  if (value.isEmpty || value.length != 10) {
                                    return "Enter valid phone number";
                                  }
                                  return null;
                                },
                              )),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    child: FilledButton(
                      width: size.width * 0.8,
                      height: size.height * 0.06,
                      title: 'CONTINUE',
                      onPressed: () {
                        if (loginProvider.formKey.currentState.validate()) {
                          Navigator.of(context)
                              .pushNamed(RouteConstants.loginPromoCodeUI,arguments: loginProvider.phoneNoController.text);
                        } else {
                          showToast('Enter valid phone number',color: ColorConstant.red);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
