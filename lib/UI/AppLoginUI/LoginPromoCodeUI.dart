import 'package:firebase_auth/firebase_auth.dart';
import 'package:nirala/Components/AppHeader/AppHeader.dart';
import 'package:nirala/Components/ShowToast/ShowToast.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Provider/LoginProvider/LoginProvider.dart';
import 'package:nirala/Services/CheckUserAddressService/CheckUserAddress.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:provider/provider.dart';

class LoginPromoCodeUI extends StatefulWidget {
  final String userPhoneNumber;

  LoginPromoCodeUI({this.userPhoneNumber});
  @override
  State<LoginPromoCodeUI> createState() => _LoginPromoCodeUIState();
}

class _LoginPromoCodeUIState extends State<LoginPromoCodeUI> {
  final TextEditingController _pinPutController = TextEditingController();
  String verificationCode;
  final FocusNode _pinPutFocusNode = FocusNode();

  final BoxDecoration pinPutDecoration = BoxDecoration(
    color: ColorConstant.white,
    borderRadius: BorderRadius.circular(10.0),
    border: Border.all(
      color:ColorConstant.appMainColor,
    ),
  );

  verifyUerLoginPhoneNumber() async {
    print(widget.userPhoneNumber);
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: '+92${widget.userPhoneNumber}',
        verificationCompleted: (PhoneAuthCredential credential) async {
          await FirebaseAuth.instance
              .signInWithCredential(credential)
              .then((value) async {
            if (value.user != null) {
              CheckUserAddress.checkUserAddress(context);
            }
          });
        },
        verificationFailed: (FirebaseAuthException e) {
        },
        codeSent: (String verficationID, int resendToken) {
         setState(() {
           verificationCode = verficationID;
         });
        },
        codeAutoRetrievalTimeout: (String verificationID) {
         setState(() {
           verificationCode = verificationID;
         });
        },
        timeout: Duration(seconds: 120));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    verifyUerLoginPhoneNumber();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Consumer<LoginProvider>(builder: (context, loginProvider, child) {
      return Scaffold(
        body: Stack(
          children: [
            Container(
              width: size.width * 1.0,
              height: size.height * 1.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/bkg.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        AppHeader(
                          title: 'PromoCode',
                          onPressedLeadingButton: () =>
                              Navigator.of(context).pop(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 50),
                          alignment: Alignment.center,
                          child: Text(
                            'We need to verify your phone number',
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontFamily: 'heading'),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: size.width * 0.9,
                          child: Text(
                            'We have sent you an SMS with a code to number  ' +
                                '+92' +
                                loginProvider.phoneNoController.text,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: PinPut(
                            fieldsCount: 6,
                            textStyle: FCSignInPinOtpTxt,
                            eachFieldWidth: 20.0,
                            eachFieldHeight: 20.0,
                            focusNode: _pinPutFocusNode,
                            controller: _pinPutController,
                            submittedFieldDecoration: pinPutDecoration,
                            selectedFieldDecoration: pinPutDecoration,
                            followingFieldDecoration: pinPutDecoration,
                            pinAnimationType: PinAnimationType.fade,
                            onSubmit: (pin) async {
                              try {
                                await FirebaseAuth.instance
                                    .signInWithCredential(
                                        PhoneAuthProvider.credential(
                                            verificationId: verificationCode,
                                            smsCode: pin))
                                    .then((value) async {
                                  if (value.user != null) {
                                    loginProvider.updateUserLoginDetail();
                                    CheckUserAddress.checkUserAddress(context);
                                  }
                                });
                              } catch (e) {
                                FocusScope.of(context).unfocus();
                                showToast('invalid OTP',
                                    color: ColorConstant.red);
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
