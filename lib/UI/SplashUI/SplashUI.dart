import 'dart:async';
import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppImages.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';

class SplashUI extends StatefulWidget {
  @override
  _SplashUIState createState() => _SplashUIState();
}

class _SplashUIState extends State<SplashUI> {
  void startTimer() {
    Timer(
        Duration(seconds: 3),
        () => Navigator.of(context)
            .pushReplacementNamed(RouteConstants.welcomeLoginUI));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstant.black,
      body: Center(
        child: Image.asset(
          AppImages.appLogo,
          width: MediaQuery.of(context).size.width * 0.8,
        ),
      ),
    );
  }
}
