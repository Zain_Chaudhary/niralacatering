import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:nirala/Components/AppDrawer/AppDrawer.dart';
import 'package:nirala/Components/AppHeader/AppHeader.dart';
import 'package:nirala/Components/Buttons/FilledButton.dart';
import 'package:nirala/Components/CustomImages/NetworkImage.dart';
import 'package:nirala/Components/ShowToast/ShowToast.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Components/TextFields/CustomTextField.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:nirala/Provider/AddressProvider/AddressProvider.dart';
import 'package:nirala/Services/SharedPreferenceService/SharedPreferencesService.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:provider/provider.dart';



final database = FirebaseDatabase.instance.reference();

final FirebaseAuth _auth = FirebaseAuth.instance;

class EditAccountUI extends StatefulWidget {
  final bool showAppBar;
  EditAccountUI({this.showAppBar=true});
  @override
  State<EditAccountUI> createState() => _EditAccountUIState();
}

class _EditAccountUIState extends State<EditAccountUI> {

  final nameController = TextEditingController();
  final aboutController = TextEditingController();
  final emailController = TextEditingController();
  final numberController = TextEditingController();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();



  String phoneNumber;

  String userid;

  var number = "userphonenumber";

  List data;

  void getPhoneNumber() {
    User user = _auth.currentUser;
    String id = user.uid;
    database.child(number).child(id).once().then((DataSnapshot snapshot) {
      var data = snapshot.value;

      setState(() {
        phoneNumber = data['userphonenumber'].toString();
        userid = data['userid'].toString();
      });
    });
  }

  //getting image from gallery



//uploading user info in database
  Future uploadUserInfo(String url) async {
    String uName = nameController.text;
    String uAbout = aboutController.text;
    String uEmail = emailController.text;
    String address = _address.text;
    String street = _street.text;
    String block = _block.text;
    String town = _town.text;


    User user = _auth.currentUser;
    String id = user.uid;

    if (uName.isNotEmpty && uAbout.isNotEmpty && uEmail.isNotEmpty && address.isNotEmpty) {
      database.child('UserInfo').child(id).update({
        'Name': uName,
        'About': uAbout,
        'Email': uEmail,
        'uid': id,
        'ProfilePic': url??"https://www.pinclipart.com/picdir/middle/27-278768_free-vector-graphic-person-clip-art-png-download.png",
        'useraddress': address,
        'userstreet': street,
        'userblock': block,
        'usertown': town,
        'HomeorWork': place,
      });
      if (database != null) {
        Navigator.of(this.context)
            .push(MaterialPageRoute(builder: (context) => AppDrawer()));
        _updateUserAddress();
      } else {
        showToast('Data Not Enter SuccessFully');
      }
    }
    else{

    }
  }


  bool datauser = false;
  var uInfo = "UserInfo";
  TextEditingController _address = TextEditingController();
  TextEditingController _street = TextEditingController();
  TextEditingController _block = TextEditingController();
  TextEditingController _town = TextEditingController();

  String place;

  void _updateUserAddress() {
    String address = _address.text;
    String street = _street.text;
    String block = _block.text;
    String town = _town.text;
    User user = _auth.currentUser;
    String id = user.uid;

    if (address.isNotEmpty && street.isNotEmpty && block.isNotEmpty && town.isNotEmpty) {
      Provider.of<AddressProvider>(this.context,listen: false).setUserAddress(address);
      database.child("UserAddress").child(id).update({
        'useraddress': address??"",
        'userstreet': street??"",
        'userblock': block??"",
        'usertown': town??"",
        'userid': id,
        'HomeorWork': place??"",
      }).then((_) async {
        if (address.isNotEmpty) {
          await SharedPreferencesService().setString(KeysConstants.userAddress, address);
        }
        showToast('Updated');
        _address.clear();
        _street.clear();
        _block.clear();
        _town.clear();
      }).catchError((onError) {});
    } else {
      showToast('Please Complete Required Field',color: ColorConstant.red);
    }
  }

  Future<void> _getCurrentUserInfo() async {
    User user = await _auth.currentUser;
    String id = user.uid;
    database.child(uInfo).child(id).once().then((DataSnapshot snapshot) {
      var data = snapshot.value;

      setState(() {
        datauser = true;
        emailController..text = data['Email'].toString()??"";
        _address..text = data['useraddress'].toString()??"";
        _block..text = data['userblock'].toString()??"";
        _street..text = data['userstreet'].toString()??"";
        _town..text = data['usertown'].toString()??"";
        nameController..text = data['Name'].toString()??"";
        aboutController..text = data['About'].toString()??"";
        Provider.of<AddressProvider>(this.context,listen: false).ImgUrl=data['ProfilePic'].toString();
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPhoneNumber();
    _getCurrentUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: ColorConstant.appMainColor,
      body: Consumer<AddressProvider>(
        builder: (context, addressProvider,child) {
          return SafeArea(
            child: ListView(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               widget.showAppBar? Container(
                  height: size.height * 0.1,
                  child: AppHeader(
                    title: 'User Information',
                    onPressedLeadingButton:()=> Navigator.of(context).pop(),
                  ),
                ):Container(
                 height: size.height * 0.1,
               ),
                Container(
                  height:size. height * 0.2,
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                          child: ClipOval(
                              child: addressProvider.profilePic != null
                                  ? Image.file(addressProvider.profilePic,
                                height: 100,
                                width: 100,
                                fit: BoxFit.cover,
                              )
                                  : InkWell(
                                onTap: ()=>addressProvider.openBottomSheet(context),
                                child: addressProvider.ImgUrl!=null?
                                ClipRRect(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(100)),
                                    child:  appNetworkImage(
                                        addressProvider.ImgUrl,
                                        100,
                                        100,
                                        BoxFit.cover,
                                        loadingColor: ColorConstant.white
                                    )
                                )
                                    :Container(
                                  child: Stack(
                                    children: [
                                      Center(
                                        child: CircleAvatar(
                                          radius: size.height * 0.08,
                                          backgroundColor: Colors.black,
                                          backgroundImage:
                                          AssetImage('assets/man.png'),
                                        ),
                                      ),
                                      Center(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              top: size.height * 0.045),
                                          child: IconButton(
                                            color: Color(0xFFFFBD2F),
                                            onPressed: ()=>addressProvider.openBottomSheet(context),
                                            icon: Icon(
                                                Icons.camera_alt_sharp),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width:size. width * 1.0,
                  height: size.height * 0.7,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: Form(
                    key: formKey,
                    child: ListView(
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Container(
                                  width: size.width * 0.8,
                                  margin: EdgeInsets.only(top: 15),
                                  child: CustomTextFormField(
                                    textFieldBorder: false,
                                    controller: nameController,
                                    labelText: 'User Name',
                                    inputType: TextInputType.text,
                                    suffixWidget: Icon(Icons.edit,size: 20,),
                                    validator: (String value)=> value.isEmpty?"Required field":null,
                                  )
                              ),
                              Container(
                                  width: size.width * 0.8,
                                  margin: EdgeInsets.only(top: 15),
                                  child: CustomTextFormField(
                                    textFieldBorder: false,
                                    controller: aboutController,
                                    labelText: 'About',
                                    inputType: TextInputType.text,
                                    suffixWidget: Icon(Icons.edit,size: 20,),
                                  )
                              ),
                              Container(
                                  width: size.width * 0.8,
                                  margin: EdgeInsets.only(top: 15),
                                  child: CustomTextFormField(
                                    textFieldBorder: false,
                                    controller: emailController,
                                    labelText: 'Email',
                                    inputType: TextInputType.emailAddress,
                                    suffixWidget: Icon(Icons.edit,size: 20,),
                                  )
                              ),
                              Container(
                                  width: size.width * 0.8,
                                  margin: EdgeInsets.only(top: 15),
                                  child: CustomTextFormField(
                                    textFieldBorder: false,
                                    controller: _address,
                                    labelText: 'Address',
                                    inputType: TextInputType.streetAddress,
                                    suffixWidget: Icon(Icons.edit,size: 20,),
                                    validator: (String value)=> value.isEmpty?"Required field":null,
                                  )
                              ),
                              Container(
                                padding:
                                EdgeInsets.only(top: size.height * 0.05, left: size.width * 0.05,right: size.width * 0.05),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Container(
                                        width: size.width * 0.4,
                                        child: CustomTextFormField(
                                          textFieldBorder: false,
                                          controller: _street,
                                          labelText: 'Street No:',
                                          inputType: TextInputType.number,
                                          suffixWidget: Icon(Icons.edit,size: 20,),
                                          validator: (String value)=> value.isEmpty?"Required field":null,
                                        )
                                    ),
                                    SizedBox(
                                      width:size. width * 0.05,
                                    ),
                                    Container(
                                        width:size. width * 0.4,
                                        child: CustomTextFormField(
                                          textFieldBorder: false,
                                          controller: _block,
                                          labelText: 'Block:',
                                          inputType: TextInputType.streetAddress,
                                          suffixWidget: Icon(Icons.edit,size: 20,),
                                          validator: (String value)=> value.isEmpty?"Required field":null,
                                        )
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  width:size. width * 0.8,
                                  margin: EdgeInsets.only(top: 15),
                                  child: CustomTextFormField(
                                    textFieldBorder: false,
                                    controller: _town,
                                    labelText: 'Town:*',
                                    inputType: TextInputType.streetAddress,
                                    suffixWidget: Icon(Icons.edit,size: 20,),
                                    validator: (String value)=> value.isEmpty?"Required field":null,
                                  )
                              ),
                              Container(
                                margin:
                                EdgeInsets.only(top: size.height * 0.05, left: size.width * 0.05),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Text('Label As:',
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'heading',
                                          )),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: size.height * 0.02),
                                      child: Row(
                                        children: [
                                          FilledButton(
                                            width: size.width * 0.25,
                                            height:size. height * 0.06,
                                            onPressed: () {
                                              setState(() {
                                                place = 'HOME';
                                              });
                                            },
                                            btnRadius: 25,
                                            title: 'HOME',
                                            btnColor: place=='HOME' ? ColorConstant.appMainColor:ColorConstant.grey,
                                          ),
                                          SizedBox(
                                            width: size.width * 0.05,
                                          ),
                                          FilledButton(
                                            width: size.width * 0.25,
                                            height:size. height * 0.06,
                                            onPressed: () {
                                              setState(() {
                                                place = 'WORK';
                                              });
                                            },
                                            btnRadius: 25,
                                            title: 'WORK',
                                            btnColor: place=='WORK' ? ColorConstant.appMainColor:ColorConstant.grey,
                                          ),
                                          SizedBox(
                                            width: size.width * 0.05,
                                          ),
                                          FilledButton(
                                            width: size.width * 0.25,
                                            height:size. height * 0.06,
                                            onPressed: () {
                                              setState(() {
                                                place = 'Other';
                                              });
                                            },
                                            btnRadius: 25,
                                            title: 'Other',
                                            btnColor: place=='Other' ? ColorConstant.appMainColor:ColorConstant.grey,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                width: size.width * 0.8,
                                child: TextFormField(
                                  //controller: numberController,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    enabled: false,
                                    hintText: phoneNumber,
                                    hintStyle: FCEditAccounttext,
                                    labelText: 'Mobile Number',
                                    labelStyle: FCEditAccountStyle,
                                    floatingLabelBehavior:
                                    FloatingLabelBehavior.always,
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(bottom: 40,top: 20),
                          child: FilledButton(
                            width: size.width * 0.8,
                            height: size.height * 0.06,
                            onPressed: (){
                              if(formKey.currentState.validate()){
                                uploadUserInfo(addressProvider.ImgUrl);
                              }else{
                                showToast('Please Complete Required Field',color: ColorConstant.red);
                              }
                            },
                            title: 'SAVE',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        }
    ),
    );
  }
}

