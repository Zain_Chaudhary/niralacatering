import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:nirala/Components/CustomImages/NetworkImage.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:nirala/UI/FavFoodUI/UserFavFoodUI.dart';
import 'package:nirala/UI/TrackerOrder/TrackerOrder.dart';
import 'package:nirala/UI/UserAccountUI/EditAccountUI.dart';
import 'package:nirala/UI/UserAccountUI/UserAddressUI.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';

final database = FirebaseDatabase.instance.reference();
final FirebaseAuth _auth = FirebaseAuth.instance;

class UserAccountUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      body: MyAccount(),
    );
  }
}

class MyAccount extends StatefulWidget {
  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  double height;
  double width;
  var uInfo = "UserInfo";
  String userImg;
bool isLoading=true;
  String userNam;
  String userAbout;

  List data;

  void getCurrentUserInfo() {
    User user = _auth.currentUser;
    String id = user.uid;
    database.child(uInfo).child(id).once().then((DataSnapshot snapshot) {
      var data = snapshot.value;

      setState(() {
        isLoading=false;
        userImg = data['ProfilePic'].toString();
        userNam = data['Name'].toString();
        userAbout = data['About'].toString();
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;

    return  SafeArea(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(top: height * 0.03),
                child: Column(
                  children: [
                    Container(
                      width: width * 1.0,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ClipRRect(
                              borderRadius:
                              BorderRadius.all(Radius.circular(100)),
                              child:  userImg ==null
                                  ? Image.asset('assets/man.png',
                                height: 100,
                                width: 100,
                                fit: BoxFit.cover,
                              )
                                  : appNetworkImage(
                                  userImg,
                                  100,
                                  100,
                                  BoxFit.cover,
                              )
                          ),
                          SizedBox(
                            width: width * 0.08,
                          ),
                          Container(
                            child: Column(
                              children: [
                                Container(
                                  child: Text(userNam??"", style: FCAccountTitle),
                                ),
                                Container(
                                  margin: EdgeInsets.only(),
                                  child: Text(userAbout??"",
                                      style: FCAccountsubheadingText),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.05,
                    ),
                    Container(
                      width: width * 1.0,
                      height: height * 0.25,
                      padding: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      margin: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      child: Card(
                        elevation: 20,
                        shadowColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => EditAccountUI()));
                                },
                                child: ListTile(
                                  leading: Container(
                                    width: width * 0.1,
                                    height: height * 0.1,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: Icon(
                                      Icons.person,
                                      color: ColorConstant.appMainColor,
                                    ),
                                  ),
                                  title: Text('Personal Info',
                                      style: FCAccountheadingText),
                                  trailing: IconButton(
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black54,
                                      onPressed: () {}),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => UserAddressUI()));
                                },
                                child: ListTile(
                                  leading: Container(
                                    width: width * 0.1,
                                    height: height * 0.1,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: Icon(
                                      Icons.add_location,
                                      color: ColorConstant.appMainColor,
                                    ),
                                  ),
                                  title: Text('Addresses',
                                      style: FCAccountheadingText),
                                  trailing: IconButton(
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black54,
                                      onPressed: () {}),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.03,
                    ),
                    Container(
                      width: width * 1.0,
                      height: height * 0.24,
                      margin: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      padding: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      child: Card(
                        elevation: 20,
                        shadowColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => UserFavFoodUI()));
                                },
                                child: ListTile(
                                  leading: Container(
                                    width: width * 0.1,
                                    height: height * 0.1,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: Icon(
                                      Icons.favorite,
                                      color: ColorConstant.appMainColor,
                                    ),
                                  ),
                                  title: Text('Favourite',
                                      style: FCAccountheadingText),
                                  trailing: IconButton(
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black54,
                                      onPressed: () {}),
                                ),
                              ),
                              InkWell(
                                onTap: ()=>    Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => OrderTrackerTimer())),
                                child: ListTile(
                                  leading: Container(
                                    width: width * 0.1,
                                    height: height * 0.1,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: Icon(
                                      Icons.payment,
                                      color: ColorConstant.appMainColor,
                                    ),
                                  ),
                                  title: Text('Tracker',
                                      style: FCAccountheadingText),
                                  trailing: IconButton(
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black54,
                                      onPressed: () {}),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.03,
                    ),
                    Container(
                      width: width * 1.0,
                      height: height * 0.13,
                      margin: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      padding: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      child: Card(
                        elevation: 20,
                        shadowColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              InkWell(
                                onTap: ()=> Navigator.of(context).pushReplacementNamed(RouteConstants.appLogout),
                                child: ListTile(
                                  leading: Container(
                                    width: width * 0.1,
                                    height: height * 0.1,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: Icon(
                                      Icons.logout,
                                      color: ColorConstant.appMainColor,
                                    ),
                                  ),
                                  title: Text('Sign Out',
                                      style: FCAccountheadingText),
                                  trailing: IconButton(
                                      icon: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                      color: Colors.black54,
                                      onPressed: ()=>Navigator.of(context).pushReplacementNamed(RouteConstants.appLogout)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.03,
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
