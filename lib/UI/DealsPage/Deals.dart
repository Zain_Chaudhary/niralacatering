import 'package:nirala/Components/AppLoader/AppLoader.dart';
import 'package:nirala/Components/Buttons/FilledButton.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:nirala/Provider/MenuDealsProvider/MenuDealProvider.dart';
import 'package:nirala/Services/DealServices/DealService.dart';
import 'package:nirala/UI/RestaurantMenu/SelectMenuDetail.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:provider/provider.dart';



class MenuDealsUI extends StatefulWidget {
  @override
  State<MenuDealsUI> createState() => _MenuDealsUIState();
}

class _MenuDealsUIState extends State<MenuDealsUI> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<MenuDealProvider>(context,listen: false).getMenuDealsData();
    DealService().getMenuDealsData();
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Consumer<MenuDealProvider>(
      builder: (context, menuDealProvider,child) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.white,
          body: menuDealProvider.isLoading
              ? AppLoader()
              : SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      width:size. width * 1.0,
                      height:size.  height * 0.4,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFBD2F),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(200),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: size. height * 0.07, left:size.  width * 0.05),
                      child: Text(
                        'Exclusive Deals & \nPromotion',
                        style: FCDealsTitle,
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top:size.  height * 0.20),
                        height: size. height * 0.3,
                        width:size.  width * 1.0,
                        child: ListView(
                          children: [
                            CarouselSlider(
                                items: [
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.0),
                                      border: Border.all(
                                        color: Colors.white,
                                        width: 5,
                                      ),
                                      image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/deals/deal1.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(10.0),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 5,
                                        ),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/deals/deal2.jpg'),
                                          fit: BoxFit.cover,
                                        )),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 5,
                                        ),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/deals/deal3.jpg'),
                                          fit: BoxFit.cover,
                                        )),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 5,
                                        ),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/deals/deal4.jpg'),
                                          fit: BoxFit.cover,
                                        )),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 5,
                                        ),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/deals/deal5.jpg'),
                                          fit: BoxFit.cover,
                                        )),
                                  ),
                                ],
                                options: CarouselOptions(
                                  height: size. height * 0.30,
                                  enlargeCenterPage: true,
                                  autoPlay: true,
                                  aspectRatio: 16 / 8,
                                  autoPlayCurve: Curves.easeInOutCubic,
                                  enableInfiniteScroll: true,
                                  autoPlayAnimationDuration:
                                  Duration(milliseconds: 1000),
                                  viewportFraction: 0.85,
                                )),
                          ],
                        ))
                  ],
                ),
                SizedBox(
                  height: size. height * 0.05,
                ),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: menuDealProvider.data == null ? 0 : menuDealProvider.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        width:size.  width * 0.9,
                        height: size. height * 0.28,
                        margin: EdgeInsets.only(bottom: size. height * 0.02),
                        child: Card(
                          elevation: 12,
                          shadowColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                children: [
                                  Container(
                                      margin:
                                      EdgeInsets.only(left: size. width * 0.3),
                                      child: Text(
                                        menuDealProvider.data[index]['productname'],
                                        style: FCDealsheadingText,
                                      )),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(3),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: ColorConstant.appMainColor,
                                    ),
                                    child: CircleAvatar(
                                      radius: size. height * 0.05,
                                      backgroundImage: NetworkImage(
                                        menuDealProvider.data[index]['productimg'].toString(),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: size. width * 0.6,
                                    child: Text(
                                      menuDealProvider.data[index]['productdes'],
                                      textAlign: TextAlign.left,
                                      style: FCDealssubheadingText,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                      child: Text(
                                          'Rs: ' +
                                              menuDealProvider.data[index]['productprice'] +
                                              '/-',
                                          style: FCDealsPriceText)),
                                  SizedBox(width: size. width * 0.05),
                                  FilledButton(
                                    onPressed: ()=>       Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                MenuDetail(
                                                  productname: menuDealProvider.data[index]
                                                  ['productname'],
                                                  name: menuDealProvider.data[index]
                                                  ['productcat'],
                                                  menu: KeysConstants.dealOfDay,
                                                  isDealMenu: true,
                                                ))),
                                    title: 'Order',
                                    btnRadius: 8,
                                    width: size.width*0.25,
                                    height: 40,
                                  ),
                                  SizedBox(width: size. width * 0.05),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ],
            ),
          ),
        );
      }
    );
  }
}

