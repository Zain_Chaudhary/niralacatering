import 'package:flutter/material.dart';

const FcHomeGoodMoring = TextStyle(fontSize:13,fontFamily: 'poppinTextStyle' , color: Colors.grey,  );
const FCHomeSearch = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.grey);
const FCHomeCategory = TextStyle(fontSize: 23, fontFamily: 'heading', color: Colors.black,);
const FCHomeCategoryTitle = TextStyle(fontSize:14, fontFamily: 'heading', color: Colors.black );
//AppHomePageUI Category List View



//List view ends here......

const FCHomeSuggested = TextStyle(fontSize: 20, fontFamily: 'heading', color: Colors.black, fontWeight: FontWeight.w600);
const FCHomeSuggestedSub = TextStyle(fontSize:10, fontFamily: 'poppinTextStyle', color: Color(0xFFE0E0E0),);
const FCHomeSuggestedName = TextStyle(fontSize:18, fontFamily:'heading', color: Colors.black  );
const FCHomeSuggestedDetail = TextStyle(fontSize: 12, color: Colors.black45, fontFamily: 'poppinTextStyle' );
const FCHomeSuggestedTme = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.w500);
const FCHomeSuggestedRating = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.w500);
const FCHomeDealDay = TextStyle(fontSize: 20, fontFamily: 'heading', color: Colors.black, fontWeight: FontWeight.w600);
const FCHomeDealDayName = TextStyle(fontSize:18, fontFamily:'heading', color: Colors.black  );
const FCHomeDealDayOldPrice = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.grey, fontWeight: FontWeight.w500,decoration: TextDecoration.lineThrough);
const FCHomeDealDayOff = TextStyle(fontSize: 18, fontFamily: 'poppinTextStyle', color: Color(0xFFFFBD2F), fontWeight: FontWeight.w700);
const FCHomeDealDayAward =  TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.w500);
const FcHomeDealDaybadge = TextStyle(fontSize:13, fontFamily:'heading', color: Colors.black  );
const FCHomeNear = TextStyle(fontSize:20, fontFamily: 'heading', color: Colors.black, fontWeight: FontWeight.w600);
const FCHomeNearName = TextStyle(fontSize:18, fontFamily:'heading', color: Colors.black  );
const FCHomeNearTime = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.w500);


//singal menu items


const singalmenu1 = 'assets/images/singalmenu/menucommon_image.jpg';
const FCsignalMenuheadingText = TextStyle(fontSize: 20, fontFamily: 'heading', color: Colors.black, );
const FCsignalMenusubheadingText = TextStyle(fontSize: 17, fontFamily: 'heading', color: Colors.black);
const FCsignalMenuname = TextStyle(color: Colors.white, fontSize: 30,fontFamily: 'heading',letterSpacing: 1);


//Main_Menu Page

const FCMenuTitle = TextStyle(fontSize: 20, fontFamily: 'heading', color: Colors.black,);
const FCMenuSubTitle = TextStyle(fontSize: 23, fontFamily: 'heading', color: Colors.white,);

//Show Product Page Start


const FCProductTags = TextStyle(fontSize:12,fontFamily: 'poppinTextStyle', color: Colors.black,  );
const FCProductName = TextStyle(fontSize:23,fontFamily: 'heading', color: Colors.black,  );
const FCProductRatio = TextStyle(fontSize:13,fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.w600 );
const FCProductDescTitle =  TextStyle(fontSize:17,fontFamily: 'heading', color: Colors.black,  );
const FCProductDesc =  TextStyle(fontSize:13,fontFamily: 'poppinTextStyle', color: Colors.grey  );
const FCProductIncrement = TextStyle(fontSize:17,fontFamily: 'heading', color: Colors.black,);
const FCProductPrice = TextStyle(fontSize:15,fontFamily: 'heading', color: Colors.black,);
const FCProductPriceRs = TextStyle(fontSize:15,fontFamily: 'poppinTextStyle', color: Color(0xFFFFBD2F ),);
const FCProductAddBucket  = TextStyle(fontSize:15,fontFamily: 'heading', color: Colors.black, );


//Favorite Page start

const FCFavoriteTitle = TextStyle(fontSize:15,fontFamily: 'heading', color: Colors.grey,);

//Cart Page

const FCCartTitle = TextStyle(fontSize:30,fontFamily: 'heading', color: Colors.black, );
const FCCartName = TextStyle(fontSize:15,fontFamily: 'heading', color: Colors.black, );
const FCCartPrice = TextStyle(fontSize:12,fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCCartQuantity = TextStyle(fontSize:11,fontFamily: 'poppinTextStyle', color: Colors.black54, );
const FCCartCheckOut = TextStyle(fontSize:15,fontFamily: 'heading', color: Colors.black, );

//Favourite Page
const FCFavuriteTitle = TextStyle(fontSize:30,fontFamily: 'heading', color: Colors.black, );

const welcomeimg = 'assets/welcome.png';
const FCSignInStartImg = 'assets/images/signinbg/signinstart.png';
const FCSignInlogo = 'assets/welcome.png';
const FCSignInButtonTitle = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', fontWeight: FontWeight.bold, color: Color(0xFFFFBD2F));
const FCGuestButtonTitle = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', fontWeight: FontWeight.bold, color: Colors.black);
//Sign In With with phone number .....Enter Number Page

const FCSignInPhoneImg = 'assets/images/signinbg/signinphoneverify.png';
const FCSignInSubtitle = TextStyle(fontSize: 18, fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCSignInEnterNo =TextStyle(fontSize: 13, fontFamily: 'heading', color: Colors.black,);
const FCSignInSend = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle',  color: Colors.black);
const FCSignInPinTitle = TextStyle(fontSize: 25, fontFamily: 'heading', color: Colors.black, );
const FCSignInPinOtp = TextStyle(fontSize: 20, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.w700);
const FCSignInPinOtpTxt = TextStyle(fontSize: 20.0, color: Color(0xFFFFBD2F), fontFamily: 'poppinTextStyle', );
const FCSignInPinImg = 'assets/images/signinbg/signinPin.png';





//DealsPage
const FCDealsTitle = TextStyle(fontSize: 25, fontFamily: 'heading', color: Colors.black,);
const FCDealsheadingText = TextStyle(fontSize: 20, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.bold);
const FCDealssubheadingText = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black,);
const FCDealsPriceText = TextStyle(fontSize: 15, color: Colors.black,fontFamily: 'poppinTextStyle');


// TakeAddress

const FCCheckoutname = TextStyle(fontSize: 25,letterSpacing: 1, fontFamily: 'heading', color: Colors.black, fontWeight: FontWeight.bold);
const FCAddressUserName = TextStyle(fontSize: 18, fontFamily: 'heading', color: Colors.black,);

// Account Page
const FCAccountTitle = TextStyle(fontSize: 25, fontFamily: 'heading', color: Colors.black, );
const FCAccountheadingText = TextStyle(fontSize: 18, fontFamily: 'poppinTextStyle', color: Colors.black);
const FCAccountsubheadingText = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Color(0xFFFFBD2F));

//Edit Account Page
const FCEditAccountTitle = TextStyle(fontSize: 25, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.bold);
const FCEditAccountStyle = TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w600, fontFamily: 'heading',);
const FCEditAccounttext = TextStyle(fontSize: 15, height:1, color: Colors.black38, fontFamily: 'poppinTextStyle' );
const FCMyAccountBtn1 = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.bold);

//Edit Address Page
const FCMyAddress = TextStyle(fontFamily: 'heading', color: Colors.black,);
const FCSaveButton = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', fontWeight: FontWeight.bold, color: Colors.black);
const FCMyAddressTittle = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.bold);
const FCMyAddressSubTitle = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black);
const FCMyAddressBtn1 = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, fontWeight: FontWeight.bold);
const FCMyAddressBtn2  = TextStyle(fontSize: 13, fontFamily: 'poppinTextStyle', color: Colors.black);
const FCMyAddressLabel= TextStyle(color: Colors.black87, fontSize: 13, fontWeight: FontWeight.w600, fontFamily: 'heading',);


//Event Page
const FCEventTitle = TextStyle(fontSize: 20, fontFamily: 'heading', color: Colors.black, );
const FCEventBtn =  TextStyle(color: Colors.black,fontFamily: 'poppinTextStyle',fontSize: 18);



const cardImage='assets/images/svgImages/symbol.svg';

const FCSummaryAppbarTitle  =TextStyle(fontSize: 18, fontFamily: 'heading', color: Colors.black, );
const FCSummaryHeading = TextStyle(fontSize: 17, fontFamily: 'heading', color: Colors.black, );
const FCSummaryAddressHead = TextStyle(fontSize: 15, fontFamily: 'heading', color: Colors.black, );
const FCSummaryAddress = TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCSummaryAddressChange = TextStyle(fontSize: 15, fontFamily: 'heading', color: Colors.black, );
const FCSummaryItems =TextStyle(fontSize: 17, fontFamily: 'heading', color: Colors.black, );
const FCSummaryOrderName =   TextStyle(fontSize: 17, fontFamily: 'heading', color: Colors.black, );
const FCSummaryOrderPrice =   TextStyle(fontSize: 15, fontFamily: 'heading', color: Colors.black, );
const FCSummaryTotalPriceHead =   TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCSummaryTotalPriceVal =   TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCSummaryShippingHead =   TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCSummaryShippingVal =   TextStyle(fontSize: 15, fontFamily: 'poppinTextStyle', color: Colors.black, );
const FCSummaryPayableHead =   TextStyle(fontSize: 18, fontFamily: 'heading', color: Colors.black, );
const FCSummaryPayableVal =   TextStyle(fontSize: 15, fontFamily: 'heading', color: Colors.black, );
const FCSummaryPaymentHead =   TextStyle(fontSize: 17, fontFamily: 'heading', color: Colors.black, );
const FCSummaryConfirm =   TextStyle(fontSize: 18, fontFamily: 'heading', color: Colors.black, );