import 'package:flutter/material.dart';
import 'package:nirala/Components/AppDrawer/AppDrawer.dart';
import 'package:nirala/Components/AppLogout/AppLogout.dart';
import 'package:nirala/UI/AppLoginUI/LoginPromoCodeUI.dart';
import 'package:nirala/UI/AppLoginUI/LoginUI.dart';
import 'package:nirala/UI/AppLoginUI/WelcomeLoginUI.dart';
import 'package:nirala/UI/SplashUI/SplashUI.dart';
import 'package:nirala/UI/UserAccountUI/EditAccountUI.dart';
import 'package:nirala/UI/UserCartUI/UserOrderCartUI.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';
import 'package:page_transition/page_transition.dart';
import 'package:nirala/Utils/Constant/globals.dart' as globals;

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final animationDuration =
        Duration(milliseconds: globals.pageTransitionDuration);
    final routeArgs = settings.arguments;
    switch (settings.name) {
      case RouteConstants.initialRoute:
        return PageTransition(
            child: SplashUI(),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.welcomeLoginUI:
        return PageTransition(
            child: AppWelcomeLoginUI(),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.appLoginUI:
        return PageTransition(
            child: AppLoginUI(),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.loginPromoCodeUI:
        return PageTransition(
            child: LoginPromoCodeUI(userPhoneNumber: routeArgs,),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.appDrawer:
        return PageTransition(
            child: AppDrawer(),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.userOrderMenuCartUI:
        return PageTransition(
            child: UserOrderMenuCartUI(),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.userEditAddress:
        return PageTransition(
            child: EditAccountUI(showAppBar: routeArgs,),
            type: PageTransitionType.bottomToTop,
            duration: animationDuration);
      case RouteConstants.appLogout:
        return PageTransition(
            child: AppLogout(),
            type: PageTransitionType.fade,
            duration: animationDuration);

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
