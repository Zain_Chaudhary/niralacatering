class RouteConstants {
  static const String initialRoute = '/';
  static const String welcomeLoginUI = 'welcome_Login_UI';
  static const String appLoginUI = 'App_Login_UI';
  static const String loginPromoCodeUI = 'Login_Promo_Code_UI';
  static const String appDrawer = 'App_Drawer';
  static const String userOrderMenuCartUI = 'User_Order_Menu_Cart_UI';
  static const String userEditAddress = 'User_Edit_Address';
  static const String appLogout = 'App_Logout';
}
