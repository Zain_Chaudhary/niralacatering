import 'package:flutter/material.dart';

class ColorConstant{
  static Color appMainColor=Color(0xFFFFBD2F);
  static Color black=Colors.black;
  static Color red=Colors.red;
  static Color green=Colors.green;
  static Color grey=Colors.grey;
  static Color white=Colors.white;
  static Color transparent=Colors.transparent;
}