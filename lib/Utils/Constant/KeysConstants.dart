class KeysConstants{
  static const String appName = 'Nirala Catering';
  static const String dealOfDay = 'dealofday';
  static const String menuData = 'menu';
  static const String userAddress = 'userAddress';
}