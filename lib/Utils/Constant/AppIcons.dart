import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppIcons{
  static const IconData play = FontAwesomeIcons.play;
  static const IconData send = FontAwesomeIcons.paperPlane;
  static const IconData whatsapp = FontAwesomeIcons.whatsapp;
  static const IconData arrowBack = Icons.arrow_back_ios_new_sharp;
  static const IconData arrowDown = Icons.keyboard_arrow_down_rounded;
  static const IconData eyeOpen = Icons.visibility;
  static const IconData eyeClosed = Icons.visibility_off;
  static const IconData mail = Icons.mail;
  static const IconData facebook = Icons.facebook;
  static const IconData person = Icons.person;
  static const IconData phone = Icons.phone_android_rounded;
  static const IconData password = Icons.vpn_key_sharp;
  static const IconData close = Icons.close;
  static const IconData home = Icons.home;
  static const IconData menu = Icons.menu;
  static const IconData search = Icons.search_rounded;
  static const IconData add = Icons.add;
  static const IconData check = Icons.check;
  static const IconData cart = Icons.shopping_basket_rounded;
  static const IconData location = Icons.my_location_rounded;
  static const IconData camera = Icons.camera_alt_outlined;
  static const IconData gallery = Icons.image_search_rounded;
  static const IconData address = Icons.add_location_rounded;
}