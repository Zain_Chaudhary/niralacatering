class AppImages{
  static const String appLogo = 'assets/images/logo.png';
  static const String appLogoRemoveBG = 'assets/images/logoRemoveBG.png';
  static const String appBanner = 'assets/images/background.png';
  static const String noImage = 'assets/images/nose_image.png';
}