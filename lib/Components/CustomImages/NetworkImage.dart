import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppImages.dart';
import 'package:shimmer/shimmer.dart';

Widget appNetworkImage(String url, double width, double height, BoxFit boxFit,
    {Color imageColor,
    Color svgColor,
      Color loadingColor,
    bool showLoadingShimmers = false,
    bool showLoading = true,
    bool showErrorWidget = true,
    Color svgBackgroundColor,
    BoxFit svgBoxFit}) {
  return (url != null && url.isNotEmpty && url.length > 4)
      ? kIsWeb
          ? Image.network(
              url,
              fit: boxFit,
    width: width,
    height: height,
    color: imageColor,
            )
          : url.substring(url.length - 3) == 'svg'
              ? Material(
                  color: svgBackgroundColor ?? ColorConstant.transparent,
                  child: SvgPicture.network(
                    url ?? "",
                    fit: svgBoxFit ?? boxFit,
                    width: width,
                    color: svgColor,
                    height: height,
                    placeholderBuilder: (BuildContext context) =>
                        showLoadingShimmers
                            ? Shimmer.fromColors(
                                direction: ShimmerDirection.ltr,
                                baseColor: Colors.grey[300],
                                highlightColor: Colors.grey[100],
                                child: Container(
                                  color: ColorConstant.white,
                                ))
                            : Center(
                                child: CircularProgressIndicator()
                                // AppLoader(
                                //   strokeWidth: 1.5,
                                // ),
                              ),
                  ),
                )
              : CachedNetworkImage(
                  fit: boxFit,
                  imageUrl: url ?? "",
                  width: width,
                  color: imageColor,
                  height: height,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      showLoading
                          ? showLoadingShimmers
                              ? Shimmer.fromColors(
                                  direction: ShimmerDirection.ltr,
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[100],
                                  child: Container(
                                    color: ColorConstant.white,
                                  ))
                              : Center(
                                  child: CircularProgressIndicator(
                                    color: loadingColor??ColorConstant.appMainColor,
                                  )
                                //   AppLoader(
                                //   value: downloadProgress.progress,
                                //   strokeWidth: 1.5,
                                // )
                      )
                          : SizedBox(),
                  errorWidget: (context, url, error) => showErrorWidget
                      ? Image.asset(
                          AppImages.noImage,
                          height: height,
                          width: width,
                        )
                      : SizedBox(),
                )
      : SizedBox();
}
