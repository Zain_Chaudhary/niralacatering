import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
class AppLoader extends StatefulWidget {
  final double size;
  final Color color;
  AppLoader({this.size,this.color});

  @override
  State<AppLoader> createState() => _AppLoaderState();
}

class _AppLoaderState extends State<AppLoader> {

  String loadingText='Loading...';

  void changeText(){
    Timer(Duration(seconds: 3),(){
      setState(() {
        loadingText='Internet Slow';
      });
    } );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    changeText();
  }
  @override
  Widget build(BuildContext context) {
    return    Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SpinKitCircle(
            color: widget.color??ColorConstant.appMainColor,
            size: widget.size ?? 50.0,
          ),
          Text(loadingText,style:
          TextStyle(fontSize: 18,color: Colors.black),
          ),
        ],
      ),
    );
  }
}