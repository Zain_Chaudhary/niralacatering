import 'package:firebase_auth/firebase_auth.dart';
import 'package:nirala/Components/AppBottomBar/BottomNavBar.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Components/AppLogout/AppLogout.dart';
import 'package:nirala/UI/DealsPage/Deals.dart';
import 'package:nirala/UI/Events/EventsFiles.dart';
import 'package:nirala/UI/FAQ/AppFaqUI.dart';
import 'package:nirala/UI/FeedBack/Feedback.dart';
import 'package:nirala/UI/RestaurantMenu/ResturantMainMenu.dart';
import 'package:nirala/UI/TrackerOrder/TrackerOrder.dart';
import 'package:nirala/UI/UserAccountUI/UserAccountUI.dart';
import 'package:nirala/UI/UserCartUI/UserOrderCartUI.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppImages.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';

class DrawerItem {
  String title;
  IconData icon;

  DrawerItem(this.title, this.icon);
}

class AppDrawer extends StatefulWidget {
  final drawerItems = [
    new DrawerItem(KeysConstants.appName, Icons.home_sharp),
    new DrawerItem("RestaurantMenu", Icons.restaurant_menu_sharp),
    new DrawerItem("Events", Icons.event_available_sharp),
    new DrawerItem("Deals", Icons.fastfood_sharp),
    new DrawerItem("Cart", Icons.shopping_basket_sharp),
    new DrawerItem("Tracker", Icons.pedal_bike_sharp),
    new DrawerItem("FAQ", Icons.bookmark_border_sharp),
    new DrawerItem("My Account", Icons.account_box_sharp),
    new DrawerItem("Feedback", Icons.help_sharp),
    new DrawerItem("Sign Out", Icons.logout),
  ];

  @override
  State<StatefulWidget> createState() {
    return new AppDrawerState();
  }
}

class AppDrawerState extends State<AppDrawer> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new BottomNavBar();
      case 1:
        return new ResturantMainMenu();
      case 2:
        return new FCEvents();
      case 3:
        return new MenuDealsUI();
      case 4:
        return new UserOrderMenuCartUI();
      case 5:
        return new OrderTrackerTimer();
      case 6:
        return new AppFaqUI();
      case 7:
        return new UserAccountUI();
      case 8:
        return new FeedbackUI();
      case 9:
        return new AppLogout();
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(
          d.icon,
          color: ColorConstant.appMainColor,
        ),
        title: new Text(
          d.title,
          style: TextStyle(
              fontFamily: 'heading', fontWeight: FontWeight.w700),
        ),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }

    return new Scaffold(
      appBar: new AppBar(
        elevation: 0,
        backgroundColor: ColorConstant.appMainColor,
        leading:           Container(
          decoration: BoxDecoration(
            //borderRadius: BorderRadius.all(Radius.circular(8)),
              shape: BoxShape.circle,
              border: Border.all(
                  color: ColorConstant.black
              )
          ),
            child: IconButton(
              icon: Icon(Icons.power_settings_new),
              onPressed: () async {
                await FirebaseAuth.instance.signOut();
                Navigator.of(context).pushReplacementNamed(RouteConstants.welcomeLoginUI);
              },
            ),
          ),
        title: Center(
            child: new Text(
          widget.drawerItems[_selectedDrawerIndex].title,
          style: TextStyle(color: Colors.black, fontFamily: 'heading'),
        )),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      endDrawer: Theme(
        data: Theme.of(context).copyWith(canvasColor: Colors.white),
        child: new Drawer(
          child: SingleChildScrollView(
            child: Container(
              child: new Column(
                children: <Widget>[
                  new UserAccountsDrawerHeader(
                    accountName: Container(
                      alignment: Alignment.center,
                      child: new Text(
                        "We Deliver the Best",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontFamily: 'heading'),
                      ),
                    ),
                    currentAccountPicture: ClipOval(
                      child: SizedBox.fromSize(
                        size: Size.fromRadius(48), // Image radius
                        child: Image.asset(AppImages.appLogo, fit: BoxFit.cover),
                      ),
                    ),
                    accountEmail: null,
                    decoration: BoxDecoration(
                      color: ColorConstant.appMainColor,
                      image: DecorationImage(
                          image: AssetImage(AppImages.appBanner), fit: BoxFit.cover),
                    ),
                  ),
                  new Column(children: drawerOptions)
                ],
              ),
            ),
          ),
        ),
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}
