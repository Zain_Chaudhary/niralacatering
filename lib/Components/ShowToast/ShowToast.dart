import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';


showToast(String message, {Color color}) {
  return Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 2,
      backgroundColor: color ?? ColorConstant.appMainColor,
      textColor: Colors.white,
      fontSize: 14.0);
}

