
import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppIcons.dart';

class AppHeader extends StatelessWidget {
  final String title;
  final String step;
  final VoidCallback onPressedLeadingButton;
  final bool centerTitle;

  AppHeader(
      {this.title,
        this.step,
        this.onPressedLeadingButton,
        this.centerTitle = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: centerTitle
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.start,
        children: [
      Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
          //borderRadius: BorderRadius.all(Radius.circular(8)),
            shape: BoxShape.circle,
            border: Border.all(
                color: ColorConstant.black
            )
        ),
        child: IconButton(
            iconSize: 15,
            icon: Icon(AppIcons.arrowBack),
            color: ColorConstant.black,
            onPressed: onPressedLeadingButton ?? () {}),
      ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              title ?? "",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: ColorConstant.black, fontSize: 18),
            ),
          ),
          SizedBox(
            height: 40,
            width: 40,
          ),

        ],
      ),
    );
  }
}
