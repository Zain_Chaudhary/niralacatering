import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:nirala/UI/AppLoginUI/WelcomeLoginUI.dart';

class AppLogout extends StatefulWidget {
  @override
  _AppLogoutState createState() => _AppLogoutState();
}

class _AppLogoutState extends State<AppLogout> {


  Future<void> _logout() async {
   await FirebaseAuth.instance.signOut();
   Navigator.pushAndRemoveUntil(
       context,
       MaterialPageRoute(builder: (context) => AppWelcomeLoginUI()),
           (route) => false);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _logout();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
    );
  }

}
