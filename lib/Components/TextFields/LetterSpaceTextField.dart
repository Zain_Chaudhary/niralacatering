import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/HelpFunction/CardNumberSpace.dart';


class LetterSpaceTextFormField extends StatelessWidget {
  final String hintText;
  final String obscureCharacter;
  final bool isEnable, isObscure;
  final TextInputType inputType;
  final TextEditingController controller;
  final FocusNode node;
  final Function validator, onFieldSubmit, onChange;
  final TextInputAction inputAction;
  final bool readOnly;
  final Widget suffixWidget, prefixWidget;
  final Function() onTap;
  final VoidCallback onPressedLeadingIcon;
  final String labelText;
  final double padding;
  final int numberLength;
  final int maxLines;
  final String initialValue;
  final Color cursorColor;

  LetterSpaceTextFormField({this.validator,
    this.initialValue,
    this.cursorColor,
    this.inputAction,
    this.isEnable = true,
    this.isObscure = false,
    this.obscureCharacter,
    this.onFieldSubmit,
    this.prefixWidget,
    this.hintText,
    this.inputType,
    this.controller,
    this.numberLength,
    this.node,
    this.onChange,
    this.onTap,
    this.suffixWidget,
    this.readOnly = false,
    this.onPressedLeadingIcon,
    this.labelText,
    this.maxLines = 1,
    this.padding = 0.0});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        TextFormField(
          style: TextStyle(fontSize: 16.0, color: Colors.white),
          cursorColor: cursorColor ?? Colors.black,
          onTap: onTap,
          enabled: isEnable,
          maxLength:numberLength,
          obscureText: isObscure,
          obscuringCharacter: obscureCharacter ?? "1",
          focusNode: node,
          onFieldSubmitted: onFieldSubmit,
          initialValue: initialValue,
          controller: controller,
          textInputAction: inputAction,
          keyboardType: inputType,
          maxLines: maxLines,
          readOnly: readOnly,
          inputFormatters: [CustomInputLetterSpaceFormatter()],
          decoration: InputDecoration(
              fillColor: ColorConstant.transparent,
              suffixIcon: suffixWidget ?? null,
              hintText: hintText,
              labelText: labelText,
              labelStyle:
              TextStyle(color: ColorConstant.appMainColor, fontSize: 15.0),
              hintStyle: TextStyle(color: Colors.white, fontSize: 15.0),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.appMainColor),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.appMainColor),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.appMainColor),
              ),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              errorStyle: TextStyle(color: Colors.white, height: 1.1),
              errorMaxLines: 3,
              // contentPadding: EdgeInsets.only(left: 10.0, top: padding),
              // fillColor: Colors.white,
              filled: true),
          validator: validator,
          onChanged: onChange,
          onSaved: (String val) {
            controller.text = val;
          },
        ),
        // suffixWidget != null
        //     ? Align(alignment: Alignment.centerRight, child: suffixWidget)
        //     : SizedBox()
      ],
    );
  }
}
