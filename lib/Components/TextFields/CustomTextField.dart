import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';

class CustomTextFormField extends StatelessWidget {
  final String hintText;
  final String obscureCharacter;
  final bool isEnable, isObscure, textFieldBorder;
  final TextInputType inputType;
  final TextEditingController controller;
  final FocusNode node;
  final Function validator, onFieldSubmit, onChange;
  final TextInputAction inputAction;
  final bool readOnly;
  final Widget suffixWidget, prefixWidget;
  final Function() onTap;
  final VoidCallback onPressedLeadingIcon;
  final String labelText;
  final double padding;
  final int maxLines;
  final String initialValue;
  final Color cursorColor;

  CustomTextFormField(
      {this.validator,
      this.initialValue,
      this.cursorColor,
      this.inputAction,
      this.isEnable = true,
      this.textFieldBorder = true,
      this.isObscure = false,
      this.obscureCharacter,
      this.onFieldSubmit,
      this.prefixWidget,
      this.hintText,
      this.inputType,
      this.controller,
      this.node,
      this.onChange,
      this.onTap,
      this.suffixWidget,
      this.readOnly = false,
      this.onPressedLeadingIcon,
      this.labelText,
      this.maxLines = 1,
      this.padding = 0.0});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        TextFormField(
          style: TextStyle(fontSize: 16.0, color: ColorConstant.black),
          cursorColor: cursorColor ?? Colors.black,
          onTap: onTap,
          autofocus: false,
          enabled: isEnable,
          obscureText: isObscure,
          obscuringCharacter: obscureCharacter ?? "1",
          focusNode: node,
          onFieldSubmitted: onFieldSubmit,
          initialValue: initialValue,
          controller: controller,
          textInputAction: inputAction,
          keyboardType: inputType,
          maxLines: maxLines,
          readOnly: readOnly,
          decoration: InputDecoration(
               fillColor: ColorConstant.transparent,
              contentPadding: EdgeInsets.symmetric(vertical: 0),
              suffixIcon: suffixWidget ?? null,
              prefixIcon: prefixWidget ?? null,
              hintText: hintText,
              labelText: labelText,
              labelStyle: TextStyle(color: ColorConstant.black, fontSize: 15.0),
              hintStyle: TextStyle(color: Colors.black, fontSize: 15.0),
              border: textFieldBorder ?OutlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.black),
              ):UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.black),
              ),
              enabledBorder: textFieldBorder
                  ? OutlineInputBorder(
                      borderSide: BorderSide(color: ColorConstant.black),
                    )
                  : UnderlineInputBorder(
                      borderSide: BorderSide(color: ColorConstant.black),
                    ),
              focusedBorder:textFieldBorder? OutlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.white),
              ):UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.black),
              ),
              errorBorder: textFieldBorder? OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red),
              ):UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.black),
              ),
              focusedErrorBorder:textFieldBorder? OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ):UnderlineInputBorder(
                borderSide: BorderSide(color: ColorConstant.black),
              ),
              errorStyle: TextStyle(color: ColorConstant.red, height: 1.1),
              errorMaxLines: 3,
              // contentPadding: EdgeInsets.only(left: 10.0, top: padding),
              // fillColor: Colors.white,
              filled: true),
          validator: validator,
          onChanged: onChange,
          onSaved: (String val) {
            controller.text = val;
          },
        ),
        // suffixWidget != null
        //     ? Align(alignment: Alignment.centerRight, child: suffixWidget)
        //     : SizedBox()
      ],
    );
  }
}
