import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/AppIcons.dart';

class CustomTextArea extends StatelessWidget {
  final String hintText;
  final bool isButtonEnable;
  final TextInputType inputType;
  final TextEditingController controller;
  final FocusNode node;
  final Function validator, onFieldSubmit, onChange;
  final TextInputAction inputAction;
  final bool readOnly;
  final Widget suffixWidget, prefixWidget;
  final Function() onTap;
  final VoidCallback onPressedLeadingIcon;
  final String labelText;
  final double padding;
  final double borderRadius;
  final int maxLines;
  final double height;
  final double width;
  final String initialValue;
  final Color cursorColor;

  CustomTextArea(
      {this.validator,
        this.initialValue,
        this.cursorColor,
        this.height,
        this.borderRadius,
        this.width,
        this.inputAction,
        this.isButtonEnable = false,
        this.onFieldSubmit,
        this.prefixWidget,
        this.hintText,
        this.inputType,
        this.controller,
        this.node,
        this.onChange,
        this.onTap,
        this.suffixWidget,
        this.readOnly = false,
        this.onPressedLeadingIcon,
        this.labelText,
        this.maxLines = 10,
        this.padding = 0.0});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return  Container(
      width: width ??size.width*0.9,
      height: height ?? 130,
      decoration: BoxDecoration(
          color: ColorConstant.black.withOpacity(0.6),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius??30))
      ),
      child: Stack(
        children: [
          TextFormField(
            controller: controller,
            style: TextStyle(color: ColorConstant.black),
            decoration: InputDecoration(
              hintText: hintText,
              hintStyle: TextStyle(color: ColorConstant.black.withOpacity(0.6)),
              fillColor: ColorConstant.white,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(borderRadius??30)),
                borderSide: BorderSide(color: ColorConstant.transparent),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(borderRadius??30)),
                borderSide: BorderSide(color: ColorConstant.transparent),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(borderRadius??30)),
                borderSide: BorderSide(color: ColorConstant.transparent),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(borderRadius??30)),
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(borderRadius??30)),
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            minLines: 6, // any number you need (It works as the rows for the textarea)
            keyboardType: TextInputType.multiline,
            maxLines: maxLines,
          ),
          isButtonEnable?Align(
            alignment: Alignment.bottomRight,
            child: Container(
              height: 40,
              width: 40,
              margin: EdgeInsets.only(right: 10,bottom: 10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
               // gradient: GradientConstant.defaultCardGradient(),
                shape: BoxShape.circle
              ),
              child: InkWell(
                  onTap: onTap,
                  child: Icon(AppIcons.send,color: ColorConstant.white,size: 20,))
            ),
          ):Container()
        ],
      ),
    );
  }
}
