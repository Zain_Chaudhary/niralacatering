
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';


class SearchDropDown extends StatelessWidget {
  final Function validator;
  final List<String> items;
  final String labelText;
  final void Function(dynamic value) onChanged;
  final String selectedItem;

  SearchDropDown(
      {this.validator,
        this.labelText,
        this.items,
        this.onChanged,
        this.selectedItem,
      });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Theme(
        data: ThemeData(
          textTheme: TextTheme(subtitle1: TextStyle(color: Colors.white)),
        ),
        child: DropdownSearch<String>(

          dropdownSearchDecoration: InputDecoration(
            labelStyle: TextStyle(color: ColorConstant.appMainColor),
              contentPadding:
              EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: ColorConstant.appMainColor),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ColorConstant.appMainColor),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: ColorConstant.appMainColor),
            ),
            errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedErrorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            errorStyle: TextStyle(color: Colors.white, height: 1.1),
          ),
          validator: validator,
            mode: Mode.DIALOG,
            showAsSuffixIcons: true,
            dropDownButton: Icon(  Icons.keyboard_arrow_down,
              color: ColorConstant.white,
              size: 25,),
            popupBackgroundColor: ColorConstant.white,
    popupShape: RoundedRectangleBorder(
      // side: BorderSide(
      //   color: ColorConstants.skyBlue,
      //   width: 2
      // ),
    borderRadius: BorderRadius.all(Radius.circular(8)
    ),),
            showSearchBox: true,
            showSelectedItems: true,
            items: items,
            searchFieldProps: TextFieldProps(
              cursorColor: ColorConstant.black,
              style: TextStyle(color: ColorConstant.black),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10),
                hintText: 'Search',
                helperStyle: TextStyle(color: ColorConstant.black)
              )
            ),
            label: labelText,
            onChanged: onChanged,
          selectedItem: selectedItem,
        ),
      ),
    );
  }
}