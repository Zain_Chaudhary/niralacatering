import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:nirala/UI/AppHomePageUI/HomePageUI.dart';
import 'package:nirala/UI/FavFoodUI/UserFavFoodUI.dart';
import 'package:nirala/UI/UserAccountUI/UserAccountUI.dart';
import 'package:nirala/UI/UserCartUI/UserOrderCartUI.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _page = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();

  final List<Widget> _widgetOptions = <Widget>[
    AppHomePageUI(),
    UserOrderMenuCartUI(),
    UserFavFoodUI(),
    UserAccountUI(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _page = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: _widgetOptions.elementAt(_page),
        ),
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: 0,
          items: <Widget>[
            Icon(
              Icons.home_sharp,
              size: 25,
              color: ColorConstant.black,
            ),
            Icon(
              Icons.shopping_basket_sharp,
              size: 25,
              color: ColorConstant.black,
            ),
            Icon(
              Icons.favorite_border_rounded,
              size: 25,
              color: ColorConstant.black,
            ),
            Icon(
              Icons.person_rounded,
              size: 25,
              color: ColorConstant.black,
            ),
          ],
          color: Colors.white,
          buttonBackgroundColor: Color(0xFFFFBD2F),
          backgroundColor: Colors.transparent,
          animationCurve: Curves.decelerate,
          animationDuration: Duration(milliseconds: 500),
          onTap: _onItemTapped,
          letIndexChange: (index) => true,
        ));
  }
}
