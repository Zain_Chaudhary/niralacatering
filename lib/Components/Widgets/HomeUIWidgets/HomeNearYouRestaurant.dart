import 'package:flutter/material.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:nirala/Models/HomePageModel/HomeNearYouRestaurant.dart';

class HomeNearYouRestaurant extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return ListView.builder(
      itemCount: homeRestaurantList.length,
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemBuilder:
          (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
              borderRadius:
              BorderRadius.circular(7)),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                    right: size.width * 0.05),
                height: size.height * 0.5,
                width:size. width * 0.6,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius
                              .circular(7),
                          image: DecorationImage(
                              image: AssetImage(
                                homeRestaurantList[index].image,
                              ),
                              fit: BoxFit
                                  .cover)),
                    ),
                    Center(
                      child: Container(
                          margin: EdgeInsets
                              .only(
                              top: size.height *
                                  0.3),
                          width: size.width * 0.5,
                          height: size.height *
                              0.09,
                          decoration: BoxDecoration(
                              color: Color(
                                  0xFFFFBD2F),
                              borderRadius:
                              BorderRadius
                                  .circular(
                                  7)),
                          child: Column(
                            mainAxisAlignment:
                            MainAxisAlignment
                                .center,
                            children: [
                              Container(
                                child: Text(
                                  homeRestaurantList[index].name,
                                  style:
                                  FCHomeNearName,
                                ),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment
                                      .center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets
                                          .only(
                                          right: size.width *
                                              0.02),
                                      child: Icon(
                                        Icons
                                            .access_time_sharp,
                                        color: Colors
                                            .black54,
                                        size: 15,
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        homeRestaurantList[
                                        index].time,
                                        style:
                                        FCHomeNearTime,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
