import 'package:flutter/material.dart';
import 'package:nirala/Components/CustomImages/NetworkImage.dart';
import 'package:nirala/Constant%20Folder/Constant%20File.dart';
import 'package:nirala/Provider/HomeProvider/HomeProvider.dart';
import 'package:nirala/UI/RestaurantMenu/SelectMenuDetail.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:rotated_corner_decoration/rotated_corner_decoration.dart';

class HomeDealMenu extends StatelessWidget {
  HomeProvider homeProvider;
  HomeDealMenu({this.homeProvider});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return  ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: homeProvider.dealMenuData == null ? 0 : homeProvider.dealMenuData
          .length,
      scrollDirection: Axis.vertical,
      itemBuilder:
          (BuildContext context, int index) {
        return InkWell(
          onTap: ()=>  Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) =>
                      MenuDetail(
                        menu: KeysConstants.dealOfDay,
                        productname: homeProvider.dealMenuData[index]['productname'],
                        name: homeProvider.dealMenuData[index]['productcat'],
                        isDealMenu: true,
                      ))),
          child: Container(
            height: size.height * 0.35,
            margin: EdgeInsets.only(
                bottom: size.height * 0.02),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7),
                topRight: Radius.circular(7),
              ),
            ),
            child: Card(
              elevation: 12,
              shadowColor: Colors.white,
              child: Column(
                crossAxisAlignment:
                CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      Container(
                        height: size.height *
                            0.25,
                        width: size.width * 0.92,
                        decoration:
                        BoxDecoration(
                          borderRadius:
                          BorderRadius.only(
                            topLeft:
                            Radius.circular(
                                7),
                            topRight:
                            Radius.circular(
                                7),
                          ),
                        ),
                        child: ClipRRect(
                          borderRadius:
                          BorderRadius.only(
                            topLeft:
                            Radius.circular(
                                7),
                            topRight:
                            Radius.circular(
                                7),
                          ),
                          child:
                          appNetworkImage(
                              homeProvider.dealMenuData[index]
                              ['productimg']
                                  .toString(),
                              size. width * 0.92,
                              size. height * 0.25,
                              BoxFit.cover),
                        ),
                      ),
                      Container(
                        padding:
                        const EdgeInsets
                            .all(40),
                        alignment:
                        Alignment.topLeft,
                        foregroundDecoration:
                        RotatedCornerDecoration(
                          badgeShadow:
                          BadgeShadow(
                              color: Colors
                                  .black,
                              elevation:
                              10),
                          color:
                          Color(0xFFFFBD2F),
                          geometry: BadgeGeometry(
                              width: 80,
                              height: 80,
                              alignment:
                              BadgeAlignment
                                  .topLeft),
                          textSpan: TextSpan(
                            text: homeProvider.dealMenuData[index][
                            'productdiscount'] +
                                '%OFF'
                                    .toString(),
                            style:
                            FcHomeDealDaybadge,
                          ),
                          labelInsets:
                          LabelInsets(
                              baselineShift:
                              5),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: size.height * 0.08,
                    child: Column(
                      mainAxisAlignment:
                      MainAxisAlignment
                          .spaceEvenly,
                      children: [
                        Container(
                          child: Text(
                            homeProvider.dealMenuData[index]
                            ['productname'],
                            style:
                            FCHomeDealDayName,
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment
                                .spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets
                                    .only(
                                    left: size.width *
                                        0.05),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons
                                          .star_half_sharp,
                                      color: Color(
                                          0xFFFFBD2F),
                                      size: 15,
                                    ),
                                    Container(
                                      child: Text(
                                        homeProvider.dealMenuData[index]
                                        [
                                        'productrating'],
                                        style:
                                        FCHomeSuggestedRating,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                      child: Image
                                          .asset(
                                        'assets/images/menu/dealday/ribbon.png',
                                        height:
                                        size. height *
                                            0.02,
                                      )),
                                  SizedBox(
                                    width:size. width *
                                        0.01,
                                  ),
                                  Container(
                                    margin: EdgeInsets
                                        .only(
                                        right: size.width *
                                            0.05),
                                    child: Text(
                                      'Best Seller of the week',
                                      style:
                                      FCHomeDealDayAward,
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
