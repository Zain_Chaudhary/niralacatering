import 'package:flutter/material.dart';
import 'package:nirala/Components/CustomImages/NetworkImage.dart';
import 'package:nirala/Provider/HomeProvider/HomeProvider.dart';
import 'package:nirala/UI/RestaurantMenu/SelectMenuDetail.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';

class HomeSuggestionMenu extends StatelessWidget {
  HomeProvider homeProvider;
  HomeSuggestionMenu({this.homeProvider});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return ListView.builder(
      itemCount: homeProvider.suggestionMenuData == null
          ? 0
          : homeProvider.suggestionMenuData.length,
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemBuilder: (BuildContext context,
          int index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context)
                .push(MaterialPageRoute(
                builder: (context) =>
                    MenuDetail(
                      productname:
                      homeProvider.suggestionMenuData[index]
                      ['productname'],
                      name: homeProvider.suggestionMenuData[index]
                      ['productcat'],
                      menu: KeysConstants.menuData,
                    )));
          },
          child: Container(
            width: size.width * 0.75,
            height: size.height * 0.4,
            padding:
            EdgeInsets.only(bottom: size.height * 0.05),
            margin:
            EdgeInsets.only(right: size.width * 0.05),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                  7),
            ),
            child: Card(
              elevation: 12,
              shadowColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(7),
                  topRight: Radius.circular(7),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    height: size.height * 0.25,
                    width:size. width * 0.75,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius
                          .only(
                        topLeft: Radius.circular(7),
                        topRight: Radius.circular(
                            7),
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius
                          .only(
                        topLeft: Radius.circular(7),
                        topRight: Radius.circular(
                            7),
                      ),
                      child: appNetworkImage(
                          homeProvider.suggestionMenuData[index]
                          ['productimg']
                              .toString(),
                          size.width * 0.75,
                          size. height * 0.25,
                          BoxFit.cover),
                    ),
                  ),
                  Container(
                    height: size.height * 0.12,
                    child: Column(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          alignment: Alignment
                              .center,
                          width: size.width * 0.6,
                          child: Text(
                            homeProvider.suggestionMenuData[index]
                            ['productname'],
                            style: TextStyle(
                                fontSize: size.width *
                                    0.05,
                                fontFamily:
                                'heading',
                                color: Colors
                                    .black),
                          ),
                        ),
                        Container(
                          alignment: Alignment
                              .center,
                          width:size. width * 0.6,
                          child: Text(
                            homeProvider.suggestionMenuData[index]
                            ['productdes'],
                            style: TextStyle(
                                fontSize:size. width *
                                    0.03,
                                color: Colors
                                    .black45,
                                fontFamily:
                                'poppinTextStyle'),
                            overflow:
                            TextOverflow.ellipsis,
                            maxLines: 2,
                            textAlign: TextAlign
                                .center,
                          ),
                        ),
                        Container(
                          alignment: Alignment
                              .center,
                          width: size.width * 0.6,
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment
                                .spaceEvenly,
                            children: [
                              Container(
                                child: Icon(
                                  Icons
                                      .access_time_sharp,
                                  color:
                                  Color(0xFFFFBD2F),
                                  size: 15,
                                ),
                              ),
                              Container(
                                child: Text(
                                    homeProvider.suggestionMenuData[
                                    index][
                                    'producttime'] +
                                        'min'
                                            .toString(),
                                    style: TextStyle(
                                        fontSize:
                                        size.width *
                                            0.03,
                                        fontFamily:
                                        'poppinTextStyle',
                                        color: Colors
                                            .black,
                                        fontWeight:
                                        FontWeight
                                            .w500)),
                              ),
                              SizedBox(
                                width:size. width *
                                    0.005,
                              ),
                              Container(
                                child: Icon(
                                  Icons
                                      .star_half_sharp,
                                  color:
                                  Color(0xFFFFBD2F),
                                  size: 15,
                                ),
                              ),
                              Container(
                                child: Text(
                                  homeProvider.suggestionMenuData[index][
                                  'productrating']
                                      .toString(),
                                  style: TextStyle(
                                      fontSize:
                                      size. width * 0.03,
                                      fontFamily:
                                      'poppinTextStyle',
                                      color:
                                      Colors.black,
                                      fontWeight:
                                      FontWeight
                                          .w500),
                                ),
                              ),
                              SizedBox(
                                width: size.width *
                                    0.005,
                              ),
                              Container(
                                child: Icon(
                                  Icons
                                      .delivery_dining,
                                  color:
                                  Color(0xFFFFBD2F),
                                  size: 15,
                                ),
                              ),
                              Container(
                                child: Text(
                                    'Free delivery',
                                    style: TextStyle(
                                        fontSize:
                                        size.width *
                                            0.03,
                                        fontFamily:
                                        'poppinTextStyle',
                                        color: Colors
                                            .black,
                                        fontWeight:
                                        FontWeight
                                            .w500)),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
