import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';



class FilledButton extends StatelessWidget {
  final String title;
  final Gradient buttonGradients;
  final double width;
  final double height;
  final double btnRadius;
  final Color textColor;
  final Color btnColor;
  final VoidCallback onPressed;

  FilledButton(
      {this.title,
      this.onPressed,
      this.width,
      this.height,
      this.textColor,
      this.btnColor,
      this.btnRadius,
      this.buttonGradients});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: btnColor??ColorConstant.appMainColor,
         // gradient: buttonGradients ?? GradientConstants.defaultBtnGradient(),
          borderRadius: BorderRadius.all(Radius.circular(btnRadius ?? 10))),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: btnColor ?? ColorConstant.transparent,
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(btnRadius ?? 10)),
          minimumSize: Size(width, height),
        ),
        onPressed: onPressed ?? () {},
        child: Text(
          title ?? "",
          maxLines: 1,
          style: TextStyle(
              color: textColor ?? ColorConstant.black,
              fontSize: 16,
              fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
