import 'package:flutter/material.dart';
import 'package:nirala/Utils/Constant/AppColors.dart';


class CustomOutlineButton extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;
  final double width;
  final Color borderColor;
  final Color textColor;
  final double height;
  final double fontSize;

  CustomOutlineButton({this.title, this.onPressed,this.width,this.height,this.fontSize,this.borderColor,this.textColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      child: ButtonTheme(
        height: height,
        minWidth: width,
        child: OutlineButton(
          shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10)),
          textColor: textColor??ColorConstant.white,
          child: Text(title ?? "",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: fontSize??15),
          ),
          borderSide: BorderSide(
              color: borderColor??ColorConstant.appMainColor, style: BorderStyle.solid,
              width: 1.5),
          onPressed: onPressed ?? () {},
        ),
      ),
    );
  }
}