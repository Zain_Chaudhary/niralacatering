class MainMenuModel{
  String name;
  String categoryImage;
  MainMenuModel({this.name,this.categoryImage});
}
List<MainMenuModel> mainMenuModelList=[
  MainMenuModel(name: 'Breakfast',categoryImage: 'assets/images/menu/breakfast.jpg'),
  MainMenuModel(name: 'Lunch',categoryImage: 'assets/images/menu/lunch.jpg'),
  MainMenuModel(name: 'Dinner',categoryImage: 'assets/images/menu/dinner.jpg'),
  MainMenuModel(name: 'BBQ',categoryImage: 'assets/images/menu/bbq.jpg'),
  MainMenuModel(name: 'FastFood',categoryImage: 'assets/images/menu/ff.jpg'),
  MainMenuModel(name: 'Naan',categoryImage: 'assets/images/menu/naan.jpg'),
  MainMenuModel(name: 'Desserts',categoryImage: 'assets/images/menu/dessert.jpg'),
  MainMenuModel(name: 'Drinks',categoryImage: 'assets/images/menu/drink.jpg'),
];