class HomePageCategoryModel{
  String name;
  String categoryImage;
  HomePageCategoryModel({this.name,this.categoryImage});
}
List<HomePageCategoryModel> homePageCategoryList=[
  HomePageCategoryModel(name: 'Breakfast',categoryImage: 'assets/images/category/cate1.jpg'),
  HomePageCategoryModel(name: 'Lunch',categoryImage: 'assets/images/category/cate2.jpg'),
  HomePageCategoryModel(name: 'Dinner',categoryImage: 'assets/images/category/cate3.jpg'),
  HomePageCategoryModel(name: 'Desserts',categoryImage: 'assets/images/category/cate4.jpg'),
  HomePageCategoryModel(name: 'FastFood',categoryImage: 'assets/images/category/cate5.jpg'),
  HomePageCategoryModel(name: 'Drinks',categoryImage: 'assets/images/menu/dinner.jpg'),
];