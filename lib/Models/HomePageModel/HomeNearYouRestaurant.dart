class HomeNearYouRestaurant{
  String name;
  String image;
  String time;
  HomeNearYouRestaurant({this.name,this.image,this.time});
}
List<HomeNearYouRestaurant> homeRestaurantList=[
  HomeNearYouRestaurant(name: 'Chinese Rice',image: 'assets/images/menu/nearyou/nearyou1.jpg',time: '20-30 min'),
  HomeNearYouRestaurant(name: 'Suji ka Halwa',image: 'assets/images/menu/nearyou/nearyou2.jpg',time: '20-30 min'),
  HomeNearYouRestaurant(name: 'Palak Paneer',image: 'assets/images/menu/nearyou/nearyou3.jpg',time: '20-30 min'),
  HomeNearYouRestaurant(name: 'Zinger Burger',image: 'assets/images/menu/nearyou/nearyou4.jpg',time: '20-30 min'),
];