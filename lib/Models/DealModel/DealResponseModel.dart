class DealResponseModel {
  List<Dealofday> dealofday;

  DealResponseModel({this.dealofday});

  DealResponseModel.fromJson(Map<dynamic, dynamic> map) {
    if (map['dealofday'] != null) {
      dealofday = <Dealofday>[];
      map['dealofday'].forEach((v) {
        dealofday.add(new Dealofday.fromJson(v));
      });
    }
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    if (this.dealofday != null) {
      data['dealofday'] = this.dealofday.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Dealofday {
  String productcalo;
  String productcat;
  String productdes;
  String productdiscount;
  String productimg;
  String productname;
  String productoldprice;
  String productprice;
  String productrating;
  String producttime;
  String productweight;

  Dealofday(
      {this.productcalo,
        this.productcat,
        this.productdes,
        this.productdiscount,
        this.productimg,
        this.productname,
        this.productoldprice,
        this.productprice,
        this.productrating,
        this.producttime,
        this.productweight});

  Dealofday.fromJson(Map<dynamic, dynamic> json) {
    productcalo = json['productcalo'];
    productcat = json['productcat'];
    productdes = json['productdes'];
    productdiscount = json['productdiscount'];
    productimg = json['productimg'];
    productname = json['productname'];
    productoldprice = json['productoldprice'];
    productprice = json['productprice'];
    productrating = json['productrating'];
    producttime = json['producttime'];
    productweight = json['productweight'];
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    data['productcalo'] = this.productcalo;
    data['productcat'] = this.productcat;
    data['productdes'] = this.productdes;
    data['productdiscount'] = this.productdiscount;
    data['productimg'] = this.productimg;
    data['productname'] = this.productname;
    data['productoldprice'] = this.productoldprice;
    data['productprice'] = this.productprice;
    data['productrating'] = this.productrating;
    data['producttime'] = this.producttime;
    data['productweight'] = this.productweight;
    return data;
  }
}
