import 'package:firebase_database/firebase_database.dart';
import 'package:nirala/Models/DealModel/DealResponseModel.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';

final database = FirebaseDatabase.instance.reference();
class DealService{
  Future<DealResponseModel> getMenuDealsData() async {
    database
        .child(KeysConstants.dealOfDay)
        .once()
        .then((DataSnapshot snapshot) {
      // Map<dynamic, dynamic> map = snapshot.value;
      // data = map.values.toList();
      DealResponseModel responseModel =
      DealResponseModel.fromJson(snapshot.value);
      print(responseModel.dealofday[1].productname.toString());
    });
  }
}