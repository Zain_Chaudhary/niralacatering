import 'package:flutter/material.dart';
import 'package:nirala/Services/SharedPreferenceService/SharedPreferencesService.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'package:nirala/Utils/Constant/RouteConstants.dart';

class CheckUserAddress{
  static checkUserAddress(BuildContext context) async {
    String userAddress =
        await SharedPreferencesService().getString(KeysConstants.userAddress);
    if(userAddress.isNotEmpty){
      Navigator.of(context).pushNamedAndRemoveUntil(RouteConstants.appDrawer,(route)=>false);
    }else{
      Navigator.of(context).pushNamedAndRemoveUntil(RouteConstants.userEditAddress,(route)=>false,arguments: false);
    }
  }
}