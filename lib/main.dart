import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nirala/Provider/AddressProvider/AddressProvider.dart';
import 'package:nirala/Provider/HomeProvider/HomeProvider.dart';
import 'package:nirala/Provider/LoginProvider/LoginProvider.dart';
import 'package:nirala/Provider/ResturantMenuProvider/RestaurantMenuProvider.dart';
import 'package:provider/provider.dart';
import 'Provider/MenuDealsProvider/MenuDealProvider.dart';
import 'Routes/Routes.dart';
import 'Utils/Constant/AppColors.dart';
import 'package:nirala/Utils/Constant/KeysConstants.dart';
import 'Utils/Constant/RouteConstants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  runApp(MyApp());
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorConstant.transparent,
      statusBarIconBrightness: Brightness.light));
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginProvider>(
          create: (context) => LoginProvider(),
        ),
        ChangeNotifierProvider<HomeProvider>(
          create: (context) => HomeProvider(),
        ),
        ChangeNotifierProvider<RestaurantMenuProvider>(
          create: (context) => RestaurantMenuProvider(),
        ),
        ChangeNotifierProvider<MenuDealProvider>(
          create: (context) => MenuDealProvider(),
        ),
        ChangeNotifierProvider<AddressProvider>(
          create: (context) => AddressProvider(),
        ),
      ],
      child: MaterialApp(
        title: KeysConstants.appName,
        theme: ThemeData(
          fontFamily: 'poppinTextStyle',
          primarySwatch: appPrimaryColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        debugShowCheckedModeBanner: false,
        initialRoute: RouteConstants.initialRoute,
        onGenerateRoute: RouteGenerator.generateRoute,
      ),
    );
  }
}

 MaterialColor appPrimaryColor =  MaterialColor(0xFFFFBD2F,
   <int, Color>{
    50: ColorConstant.appMainColor,
    100: ColorConstant.appMainColor,
    200: ColorConstant.appMainColor,
    300: ColorConstant.appMainColor,
    400: ColorConstant.appMainColor,
    500: ColorConstant.appMainColor,
    600: ColorConstant.appMainColor,
    700: ColorConstant.appMainColor,
    800: ColorConstant.appMainColor,
    900: ColorConstant.appMainColor,
  },
);
